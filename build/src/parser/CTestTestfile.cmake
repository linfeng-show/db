# CMake generated Testfile for 
# Source directory: /mnt/hgfs/aust_go/src/parser
# Build directory: /mnt/hgfs/aust_go/build/src/parser
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_parser "/mnt/hgfs/aust_go/build/bin/test_parser")
set_tests_properties(test_parser PROPERTIES  WORKING_DIRECTORY "/mnt/hgfs/aust_go/build/bin" _BACKTRACE_TRIPLES "/mnt/hgfs/aust_go/src/parser/CMakeLists.txt;14;add_test;/mnt/hgfs/aust_go/src/parser/CMakeLists.txt;0;")
