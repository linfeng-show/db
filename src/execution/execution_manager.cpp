/* Copyright (c) 2023 Renmin University of China
RMDB is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

#include "execution_manager.h"
//#include "parser/yacc.tab.h"
#include "executor_delete.h"
#include "executor_index_scan.h"
#include "execution_mu_insert.h"
#include "executor_insert.h"
#include "executor_nestedloop_join.h"
#include "executor_projection.h"
#include "executor_seq_scan.h"
#include "executor_update.h"
#include "index/ix.h"
#include "record_printer.h"
#include "common/config.h"
enum jh{
    COUNT=1,MIN=2,MAX=3,SUM=4
};

const char *help_info = "Supported SQL syntax:\n"
                   "  command ;\n"
                   "command:\n"
                   "  CREATE TABLE table_name (column_name type [, column_name type ...])\n"
                   "  DROP TABLE table_name\n"
                   "  CREATE INDEX table_name (column_name)\n"
                   "  DROP INDEX table_name (column_name)\n"
                   "  INSERT INTO table_name VALUES (value [, value ...])\n"
                   "  DELETE FROM table_name [WHERE where_clause]\n"
                   "  UPDATE table_name SET column_name = value [, column_name = value ...] [WHERE where_clause]\n"
                   "  SELECT selector FROM table_name [WHERE where_clause] ORDER BY LIMIT\n"
                   "  SELECT COUNT/SUM/MIN FROM table_name [WHERE where_clause]\n"
                   "  LOAD file_name INTO tab_name\n"
                   "  SET output_file ON/OFF\n"
                   "type:\n"
                //   在help增加了bigint和datetime两种类型
                   "  {INT | FLOAT | CHAR(n) | BIGINT | DATETIME}\n"
                   "where_clause:\n"
                   "  condition [AND condition ...]\n"
                   "condition:\n"
                   "  column op {column | value}\n"
                   "column:\n"
                   "  [table_name.]column_name\n"
                   "op:\n"
                   "  {= | <> | < | > | <= | >=}\n"
                   "selector:\n"
                   "  {* | column [, column ...]}\n";

// 主要负责执行DDL语句
void QlManager::run_mutli_query(std::shared_ptr<Plan> plan, Context *context){
    if (auto x = std::dynamic_pointer_cast<DDLPlan>(plan)) {
        switch(x->tag) {
            case T_CreateTable:
            {
                sm_manager_->create_table(x->tab_name_, x->cols_, context);
                break;
            }
            case T_DropTable:
            {
                sm_manager_->drop_table(x->tab_name_, context);
                break;
            }
            case T_CreateIndex:
            {
                sm_manager_->create_index(x->tab_name_, x->tab_col_names_, context);
                break;
            }
            case T_DropIndex:
            {
                sm_manager_->drop_index(x->tab_name_, x->tab_col_names_, context);
                break;
            }
            default:
                throw InternalError("Unexpected field type");
                break;  
        }
    }
}

// 执行help; show tables; desc table; begin; commit; abort;语句
void QlManager::run_cmd_utility(std::shared_ptr<Plan> plan, txn_id_t *txn_id, Context *context) {
    if (auto x = std::dynamic_pointer_cast<OtherPlan>(plan)) {
        switch(x->tag) {
            case T_Help:
            {
                memcpy(context->data_send_ + *(context->offset_), help_info, strlen(help_info));
                *(context->offset_) = strlen(help_info);
                break;
            }
            case T_ShowTable:
            {
                sm_manager_->show_tables(context);
                break;
            }
            case T_DescTable:
            {
                sm_manager_->desc_table(x->tab_name_, context);
                break;
            }
            case T_Transaction_begin:
            {
                //std::cout<<"T_Transaction_begin"<<std::endl;
                context->txn_ = txn_mgr_->get_transaction(*txn_id);
                txn_mgr_->commit(context->txn_, context->log_mgr_);
                context->txn_ = txn_mgr_->begin(nullptr, context->log_mgr_);
                //std::cout<<"finish begin"<<std::endl;
                *txn_id = context->txn_->get_transaction_id();
                context->txn_->set_txn_mode(true);
                break;
            }  
            case T_Transaction_commit:
            {
                //std::cout<<"T_Transaction_commit"<<std::endl;
                context->txn_ = txn_mgr_->get_transaction(*txn_id);
                txn_mgr_->commit(context->txn_, context->log_mgr_);
                break;
            }    
            case T_Transaction_rollback:
            {
                //std::cout<<"T_Transaction_rollback"<<std::endl;
                context->txn_ = txn_mgr_->get_transaction(*txn_id);
                txn_mgr_->abort(context->txn_, context->log_mgr_);
                break;
            }    
            case T_Transaction_abort:
            {
                //std::cout<<"T_Transaction_abort"<<std::endl;
                context->txn_ = txn_mgr_->get_transaction(*txn_id);
                txn_mgr_->abort(context->txn_, context->log_mgr_);
                break;
            }
            case T_ShowIndex:
            {
                // 07/11 增加showindex  这里拿不到列的信息
                sm_manager_->show_index(x->tab_name_, context);
                break;
            }
            case T_OutFile:
            {
                // 因为是直接套用的OtherPlan,所以tab_name_里面放的是控制文件写入的参数
                if(x->tab_name_ == "OFF" || x->tab_name_ == "off"){
                    IS_OUT = false;
                    //std::cout<<"outfile off!"<<std::endl;
                }else if(x->tab_name_ == "ON" || x->tab_name_ == "on"){
                    IS_OUT = true;
                    //std::cout<<"outfile on!"<<std::endl;
                }else{
                    // 其他字符
                }
                break;
            }       
            default:
                throw InternalError("Unexpected field type");
                break;                        
        }

    }
}

ColMeta QlManager:: get_col(const std::vector<ColMeta> &rec_cols, const TabCol &target) {
    ColMeta pos1;
    for( auto pos : rec_cols )
        {
            if( pos.tab_name == target.tab_name && pos.name == target.col_name)
             return pos;
        }
     
         throw ColumnNotFoundError(target.tab_name + '.' + target.col_name);
        
       return pos1;
    }


// 执行select语句，select语句的输出除了需要返回客户端外，还需要写入output.txt文件中
void QlManager::select_from(std::unique_ptr<AbstractExecutor> executorTreeRoot, std::vector<TabCol> sel_cols, 
                            Context *context) {
    int fg=0 , fgj=0;
    std::vector<std::string> captions;
    captions.reserve(sel_cols.size());
    for (auto &sel_col : sel_cols) {
        if(sel_col.JHName!=0){
            fg=1;
            fgj=sel_col.JHName;
            if(sel_col.newcolname!="0")
            captions.push_back(sel_col.newcolname);
            else
            captions.push_back(sel_col.col_name);
        }else
        captions.push_back(sel_col.col_name);
    }

    // Print header into buffer
    RecordPrinter rec_printer(sel_cols.size());
    rec_printer.print_separator(context);
    rec_printer.print_record(captions, context);
    rec_printer.print_separator(context);
    // print header into file
    std::fstream outfile;
    if(IS_OUT){
        outfile.open("output.txt", std::ios::out | std::ios::app);
        outfile << "|";
        for(int i = 0; i < captions.size(); ++i) {
            outfile << " " << captions[i] << " |";
        }
        outfile << "\n";
    }
    // Print records
    size_t num_rec = 0;
    // 执行query_plan
    if(fg){
        if(fgj==COUNT){
    for (executorTreeRoot->beginTuple(); !executorTreeRoot->is_end(); executorTreeRoot->nextTuple()) {
       // auto Tuple = executorTreeRoot->Next();
        ++num_rec;
    }
    std::vector<std::string> columns;
    columns.push_back(std::to_string(num_rec));
    // print record into buffer
        rec_printer.print_record(columns, context);
        // print record into file
        if(IS_OUT){
            outfile << "|";
            for(int i = 0; i < columns.size(); ++i) {
                outfile << " " << columns[i] << " |";
            }
            outfile << "\n";
        }

    outfile.close();
    rec_printer.print_separator(context);
    }else if (fgj==MIN)
    {
        int a=0;
        std::string min_str;
        int min_int;
        float min_float;
        std::string col_str;
        int col_int;
        float col_float;
        int TYPES;
        std::vector<std::string> columns;
        auto &prev_cols = executorTreeRoot->cols();
        //auto pos = get_col(prev_cols, sel_col);
        for (executorTreeRoot->beginTuple(); !executorTreeRoot->is_end(); executorTreeRoot->nextTuple()) {
            a++;
        auto Tuple = executorTreeRoot->Next();
        for (auto &sel_col : sel_cols) {
            //std::string col_str;
            auto col = get_col(prev_cols, sel_col);
            char *rec_buf = Tuple->data + col.offset;
            if (col.type == TYPE_INT) {
                TYPES=TYPE_INT;
                col_int=*(int *)rec_buf;
            } else if (col.type == TYPE_FLOAT) {
                TYPES=TYPE_FLOAT;
                col_float=*(float *)rec_buf;
            } else if (col.type == TYPE_STRING) {
                TYPES=TYPE_STRING;
                col_str = std::string((char *)rec_buf, col.len);
                col_str.resize(strlen(col_str.c_str()));
                }
            // *** 忘记在print这里加bigint类型了，***，调试了好几天
            // }else if(col.type == TYPE_BIGINT){
            //      col_str = std::to_string(*(int64_t *)rec_buf);
            // }
            //columns.push_back(col_str);
        }
        if(a==1){
            if(TYPES==TYPE_STRING)
            min_str=col_str;
            else if (TYPES==TYPE_FLOAT)
            {
                min_float=col_float;
            }
            else if (TYPES==TYPE_INT)
            {
                min_int=col_int;
            }
            
        }else{
            if(TYPES==TYPE_STRING)
            min_str=min_str<col_str? min_str :col_str;
            else if (TYPES==TYPE_FLOAT)
            {
                min_float=min_float<col_float? min_float :col_float;
            }
            else if (TYPES==TYPE_INT)
            {
                min_int=min_int<col_int? min_int :col_int;
            }
            
        }
    }
            if(TYPES==TYPE_STRING)
            columns.push_back(min_str);
            else if (TYPES==TYPE_FLOAT)
            {
                columns.push_back(std::to_string(min_float));
            }
            else if (TYPES==TYPE_INT)
            {
                columns.push_back(std::to_string(min_int));
            }
        // print record into buffer
        rec_printer.print_record(columns, context);
        // print record into file
        if(IS_OUT){
            outfile << "|";
            for(int i = 0; i < columns.size(); ++i) {
                outfile << " " << columns[i] << " |";
            }
            outfile << "\n";
        }
    outfile.close();
    // Print footer into buffer
    rec_printer.print_separator(context);
    }else if (fgj==MAX)
    {
        int a=0;
        std::string max_str;
        int max_int;
        float max_float;
        std::string col_str;
        int col_int;
        float col_float;
        int TYPES;
        std::vector<std::string> columns;
        auto &prev_cols = executorTreeRoot->cols();
        //auto pos = get_col(prev_cols, sel_col);
        for (executorTreeRoot->beginTuple(); !executorTreeRoot->is_end(); executorTreeRoot->nextTuple()) {
            a++;
        auto Tuple = executorTreeRoot->Next();
        for (auto &sel_col : sel_cols) {
            //std::string col_str;
            auto col = get_col(prev_cols, sel_col);
            char *rec_buf = Tuple->data + col.offset;
            if (col.type == TYPE_INT) {
                TYPES=TYPE_INT;
                col_int=*(int *)rec_buf;
            } else if (col.type == TYPE_FLOAT) {
                TYPES=TYPE_FLOAT;
                col_float=*(float *)rec_buf;
            } else if (col.type == TYPE_STRING) {
                TYPES=TYPE_STRING;
                col_str = std::string((char *)rec_buf, col.len);
                col_str.resize(strlen(col_str.c_str()));
                }
            // *** 忘记在print这里加bigint类型了，***，调试了好几天
            // }else if(col.type == TYPE_BIGINT){
            //      col_str = std::to_string(*(int64_t *)rec_buf);
            // }
            //columns.push_back(col_str);
        }
        if(a==1){
            if(TYPES==TYPE_STRING)
            max_str=col_str;
            else if (TYPES==TYPE_FLOAT)
            {
                max_float=col_float;
            }
            else if (TYPES==TYPE_INT)
            {
                max_int=col_int;
            }
            
        }else{
            if(TYPES==TYPE_STRING)
            max_str=max_str>col_str? max_str :col_str;
            else if (TYPES==TYPE_FLOAT)
            {
                max_float=max_float>col_float? max_float :col_float;
            }
            else if (TYPES==TYPE_INT)
            {
                max_int=max_int>col_int? max_int :col_int;
            }
            
        }
    }
            if(TYPES==TYPE_STRING)
            columns.push_back(max_str);
            else if (TYPES==TYPE_FLOAT)
            {
                columns.push_back(std::to_string(max_float));
            }
            else if (TYPES==TYPE_INT)
            {
                columns.push_back(std::to_string(max_int));
            }
    // print record into buffer
        rec_printer.print_record(columns, context);
        // print record into file
        if(IS_OUT){
            outfile << "|";
            for(int i = 0; i < columns.size(); ++i) {
                outfile << " " << columns[i] << " |";
            }
            outfile << "\n";
        }

    outfile.close();
    // Print footer into buffer
    rec_printer.print_separator(context);
    }
    else if (fgj==SUM)
    {

        long long sum_int=0;
        float sum_float=0;
        int si;
        float sf;
        int TYPES;
        std::vector<std::string> columns;
        auto &prev_cols = executorTreeRoot->cols();
        //auto pos = get_col(prev_cols, sel_col);
        for (executorTreeRoot->beginTuple(); !executorTreeRoot->is_end(); executorTreeRoot->nextTuple()) {
        auto Tuple = executorTreeRoot->Next();
        for (auto &sel_col : sel_cols) {
            //std::string col_str;
            auto col = get_col(prev_cols, sel_col);
            char *rec_buf = Tuple->data + col.offset;
            if (col.type == TYPE_INT) {
                TYPES=TYPE_INT;
                si = *(int *)rec_buf;
            } else if (col.type == TYPE_FLOAT) {
                TYPES=TYPE_FLOAT;
                sf= *(float *)rec_buf;
            } else {
            throw InternalError("the type can not sum");
            }
            }
            //columns.push_back(col_str);
        
        if(TYPES==TYPE_INT){
            sum_int+=si;
        }else if (TYPES==TYPE_FLOAT)
        {
            sum_float+=sf;
        }
        
    }
    if(TYPES==TYPE_INT){
            columns.push_back(std::to_string(sum_int));
        }else if (TYPES==TYPE_FLOAT)
        {
            columns.push_back(std::to_string(sum_float));
        }
    
    // print record into buffer
        rec_printer.print_record(columns, context);
        // print record into file
        if(IS_OUT){
            outfile << "|";
            for(int i = 0; i < columns.size(); ++i) {
                outfile << " " << columns[i] << " |";
            }
            outfile << "\n";
        }

    outfile.close();
    // Print footer into buffer
    rec_printer.print_separator(context);
    }
    }
    else{
         auto &prev_cols = executorTreeRoot->cols();
        //auto pos = get_col(prev_cols, sel_col); 
        //std::vector<std::string> columns;
        std::string col_str;
        std::vector<std::string> columns;
        for (executorTreeRoot->beginTuple(); !executorTreeRoot->is_end(); executorTreeRoot->nextTuple()) {
        auto Tuple = executorTreeRoot->Next();
        
        for (auto &sel_col : sel_cols) {
            //std::string col_str;
            ColMeta col = get_col(prev_cols, sel_col);
            char *rec_buf = Tuple->data + col.offset;
            if (col.type == TYPE_INT) {
                col_str = std::to_string(*(int *)rec_buf);
            } else if (col.type == TYPE_FLOAT) {
                col_str = std::to_string(*(float *)rec_buf);
            } else if (col.type == TYPE_STRING) {
                col_str = std::string((char *)rec_buf, col.len);
                col_str.resize(strlen(col_str.c_str()));
            // *** 忘记在print这里加bigint类型了，***，调试了好几天
            }else if(col.type == TYPE_BIGINT){
                 col_str = std::to_string(*(int64_t *)rec_buf);
            }else if(col.type == TYPE_DATETIME){

                uint64_t date;
                DateTime datetime;
                memcpy((char*)&date, rec_buf, sizeof(date));
                // 封装
                datetime.Decode(date);
                col_str = datetime.ToString();
            }
            columns.push_back(col_str);
        }
        // print record into buffer
        rec_printer.print_record(columns, context);
        // print record into file
        if(IS_OUT){
            outfile << "|";
            for(int i = 0; i < columns.size(); ++i) {
                outfile << " " << columns[i] << " |";
            }
            outfile << "\n";
        }
        num_rec++;
        columns.clear();
    }
    
    outfile.close();
    // Print footer into buffer
    rec_printer.print_separator(context);
    // Print record count into buffer
    RecordPrinter::print_record_count(num_rec, context);
    }
}

// 执行DML语句
void QlManager::run_dml(std::unique_ptr<AbstractExecutor> exec){
    exec->Next();
}

// 执行Load
void QlManager::load_data(std::shared_ptr<Plan> plan, Context *context){
    auto x = std::dynamic_pointer_cast<LoadPlan>(plan);
    // sm_manager_->load_data(x->tab_name_,x->file_name_,context);
    std::string absolute_path;
    // 方便调试
    // char is_debug;
    // std::cout<<"debug or run (y or n) :";
    // std::cin>>is_debug;

    // if(is_debug == 'y'){
    //     // 构建文件的绝对路径
    //     absolute_path = "../../../src/test/performance_test/table_data/" + x->file_name_ + ".csv";
    // }else{
    //     absolute_path = "../../src/test/performance_test/table_data/" + x->file_name_ + ".csv";
    // }

    absolute_path = "../../src/test/performance_test/table_data/" + x->file_name_ + ".csv";
    // 突然发现题目都是直接输入的绝对路径，上面的都多此一举了


    //std::cout<<absolute_path<<std::endl;
    // 打开文件
    std::ifstream file(absolute_path);
    if (!file.is_open()) {
        throw FileNotFoundError(x->file_name_);
    }


    // 读取文件中的数据并插入到表中
    std::string line;
    std::getline(file, line); //读取第一行并忽略，第一行为表头

    if (!sm_manager_->db_.is_table(x->tab_name_)) {
        // 检查表名是否存在
        throw TableNotFoundError(x->tab_name_);
    }
    TabMeta tab_ = sm_manager_->db_.get_table(x->tab_name_);

    std::string value;
    std::vector<Value> values; // 解析出的数据封装成Value的数组
    std::vector<std::vector<Value>> batch_values;  // 存储每批次的插入值

    while (std::getline(file, line)) {
        // 从第二行开始解析每一行的数据
        std::istringstream iss(line);
        size_t i = 0;
        values.clear();
        while (std::getline(iss, value, ',')) {
            if(i >= tab_.cols.size()){
                // 防止溢出
                break;
            }
            auto &col = tab_.cols[i++];

            Value val;
            // 这里用switch，效率高点
            switch(col.type){
                case TYPE_INT:{
                    int int_value = std::stoi(value);
                    val.set_int(int_value);
                    values.push_back(val);
                    break;
                }
                case TYPE_FLOAT:{
                    float float_value = std::stof(value);
                    val.set_float(float_value);
                    values.push_back(val);
                    break;
                }
                case TYPE_STRING:{
                    val.set_str(value);
                    values.push_back(val);
                    break;
                }
                case TYPE_DATETIME:{
                    val.set_datetime(value);
                    values.push_back(val);
                    break;
                }
                case TYPE_BIGINT:{
                    val.set_bigint(value);
                    values.push_back(val);
                    break;
                }
            }
        }
        
        batch_values.push_back(values); // 解析每行数据并添加到批量插入的值中
        if(batch_values.size() >= 1500){
            // 创建批量插入执行器
            std::unique_ptr<AbstractExecutor> ie = 
                std::make_unique<Insert_Mu_Executor>(sm_manager_, x->tab_name_ , batch_values , context);
            ie->Next();
            batch_values.clear();  // 清空当前批次的插入值
        }

        // // 创建InsertExecutor
        // std::unique_ptr<AbstractExecutor> ie =
        // std::make_unique<InsertExecutor>(sm_manager_, x->tab_name_, values, context);
        // ie->Next();
    }
    // 如果还有剩余的插入值没有达到批量插入的数量，执行最后一次批量插入操作
    if (!batch_values.empty()) {
            std::unique_ptr<AbstractExecutor> ie = 
                std::make_unique<Insert_Mu_Executor>(sm_manager_, x->tab_name_, batch_values ,context);
            ie->Next();
    }
    batch_values.clear();
    //std::cout<<"load over!"<<std::endl;
    file.close();
}
