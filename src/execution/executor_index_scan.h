/* Copyright (c) 2023 Renmin University of China
RMDB is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

#pragma once

#include "execution_defs.h"
#include "execution_manager.h"
#include "executor_abstract.h"
#include "index/ix.h"
#include "system/sm.h"
#include "executor_seq_scan.h"

class IndexScanExecutor : public AbstractExecutor {
   private:
    std::string tab_name_;                      // 表名称
    TabMeta tab_;                               // 表的元数据
    std::vector<Condition> conds_;              // 扫描条件
    RmFileHandle *fh_;                          // 表的数据文件句柄
    std::vector<ColMeta> cols_;                 // 需要读取的字段
    size_t len_;                                // 选取出来的一条记录的长度
    std::vector<Condition> fed_conds_;          // 扫描条件，和conds_字段相同

    std::vector<std::string> index_col_names_;  // index scan涉及到的索引包含的字段
    IndexMeta index_meta_;                      // index scan涉及到的索引元数据

    Rid rid_;
    std::unique_ptr<RecScan> scan_;

    SmManager *sm_manager_;
    IxIndexHandle *ih_;

   public:
    IndexScanExecutor(SmManager *sm_manager, std::string tab_name, std::vector<Condition> conds, std::vector<std::string> index_col_names,IndexMeta index_meta,
                    Context *context) {

        // std::cout<<"初始化开始"<<std::endl;
        sm_manager_ = sm_manager;
        context_ = context;
        tab_name_ = std::move(tab_name);

        // std::cout<<"QQQQQ"<<std::endl;

        tab_ = sm_manager_->db_.get_table(tab_name_);
        // std::cout<<"111"<<std::endl;
        conds_ = std::move(conds);

        // std::cout<<"222"<<std::endl;
        // index_no_ = index_no;
        index_col_names_ = index_col_names; 
        // std::cout<<"333"<<std::endl;
        index_meta_ = index_meta;
        // std::cout<<"444"<<std::endl;
        // index_meta_ = *(tab_.get_index_meta(index_col_names_));
        fh_ = sm_manager_->fhs_.at(tab_name_).get();
        // std::cout<<"555"<<std::endl;

        // ih_= sm_manager->ihs_.at(sm_manager_->get_ix_manager()->get_index_name(tab_name_, index_meta.cols)).get();
        auto index_name = sm_manager_->get_ix_manager()->get_index_name(tab_name_, index_meta_.cols);
        
        // std::cout<<"7878787878"<<std::endl;
        // std::cout<<index_meta.cols.size()<<std::endl;
        // std::cout<<index_meta_.cols.size()<<std::endl;
        
        ih_ = sm_manager_->ihs_.at(index_name).get();

        // std::cout<<"666"<<std::endl;
        cols_ = tab_.cols;
        // std::cout<<"777"<<std::endl;
        len_ = cols_.back().offset + cols_.back().len;
        // std::cout<<"888"<<std::endl;
        std::map<CompOp, CompOp> swap_op = {
            {OP_EQ, OP_EQ}, {OP_NE, OP_NE}, {OP_LT, OP_GT}, {OP_GT, OP_LT}, {OP_LE, OP_GE}, {OP_GE, OP_LE},
        };

        // std::cout<<"PPPP"<<std::endl;

        for (auto &cond : conds_) {
            if (cond.lhs_col.tab_name != tab_name_) {
                // lhs is on other table, now rhs must be on this table
                assert(!cond.is_rhs_val && cond.rhs_col.tab_name == tab_name_);
                // swap lhs and rhs
                std::swap(cond.lhs_col, cond.rhs_col);
                cond.op = swap_op.at(cond.op);
            }
        }
        fed_conds_ = conds_;
        // n=new SeqScanExecutor(sm_manager,tab_name,conds,context);
        // std::cout<<"初始化结束"<<std::endl;
        // 索引加S锁
        if(context != nullptr) {
            context->lock_mgr_->lock_shared_on_table(context->txn_, fh_->GetFd());
        }
    }
std::string getType() { return "indexScan"; }





void beginTuple() override {
    
        // n->check_runtime_conds();

        RmRecord low_k(index_meta_.col_tot_len);
        RmRecord up_k(index_meta_.col_tot_len);
        int single_of = 0;


        for(auto it = index_meta_.cols.begin();it!=index_meta_.cols.end();it++){
            const auto& col = (*it);
            
            auto [ma,mi] = get_Max_Min(col); 
            
            for (auto cond : fed_conds_) {
                
                
                if (cond.lhs_col.col_name == col.name && cond.is_rhs_val) {
                    
                    if(cond.rhs_val.type==TYPE_FLOAT&&ma.type==TYPE_INT){
                        //std::cout<<"cond.rhs_val.type==TYPE_FLOAT&&ma.type==TYPE_INT"<<std::endl;
                        cond.rhs_val.set_int((int)cond.rhs_val.float_val);
                        //std::cout<<cond.rhs_val.type<<std::endl;
                        //std::cout<<cond.rhs_val.int_val<<std::endl;
                        cond.rhs_val.init_raw(col.len);


                        // mi.set_float(mi.int_val*1.0);
                        // ma.set_float(ma.int_val*1.0);
                        // mi.init_raw(col.len);
                        // ma.init_raw(col.len);
                        
                    }

                    if(cond.rhs_val.type==TYPE_INT&&ma.type==TYPE_FLOAT){
                        //std::cout<<"cond.rhs_val.type==TYPE_INT&&ma.type==TYPE_FLOAT"<<std::endl;
                        cond.rhs_val.set_float(cond.rhs_val.int_val*1.0);
                        cond.rhs_val.init_raw(col.len);
                    }

                    // if(cond.rhs_val.type==TYPE_INT&&ma.type==TYPE_FLOAT){
                    //     std::cout<<"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"<<std::endl;
                    // }
                    // if(cond.rhs_val.type==TYPE_FLOAT&&ma.type==TYPE_INT){
                    //     // 此处应该将float转换为int
                    //     cond.rhs_val.type = TYPE_INT;
                    //     cond.rhs_val.int_val = (int)cond.rhs_val.float_val;
                        
                    //     mi.type = TYPE_INT;
                    //     ma.type = TYPE_INT;
                        
                    //     mi.int_val = (int)mi.float_val;
                    //     ma.int_val = (int)ma.float_val;
                        

                    //     std::cout<<cond.rhs_val.int_val<<std::endl;
                    //     std::cout<<mi.int_val<<std::endl;
                    //     std::cout<<ma.int_val<<std::endl;
                        


                    //     // ma.type = TYPE_FLOAT;
                    //     // mi.type = TYPE_FLOAT;
                    //     // ma.float_val = ma.int_val*1.0;
                    //     // mi.float_val = mi.int_val*1.0;
                    //     // std::cout<<cond.rhs_val.float_val<<std::endl;
                    //     // std::cout<<ma.float_val<<std::endl;
                    //     // std::cout<<mi.float_val<<std::endl;
                    //     std::cout<<"bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"<<std::endl;
                    // }
                    // if(cond.rhs_val.type==TYPE_INT&&ma.type==TYPE_INT){
                    //     std::cout<<cond.rhs_val.int_val<<std::endl;
                    //     std::cout<<ma.int_val<<std::endl;
                    //     std::cout<<mi.int_val<<std::endl;
                    //     std::cout<<"cccccccccccccccccccccccccccccc"<<std::endl;
                    // }
                    // if(cond.rhs_val.type==TYPE_FLOAT&&ma.type==TYPE_FLOAT){
                    //     std::cout<<cond.rhs_val.float_val<<std::endl;
                    //     std::cout<<ma.float_val<<std::endl;
                    //     std::cout<<mi.float_val<<std::endl;
                    //     std::cout<<"ddddddddddddddddddddddddddddd"<<std::endl;
                    // }

                    if (cond.op == OP_EQ) {
                        if (cond.rhs_val > mi) {
                            mi = cond.rhs_val;
                        }
                        if (cond.rhs_val < ma) {
                            ma = cond.rhs_val;
                        }
                    } else if (cond.op == OP_GT || cond.op == OP_GE) {
                        if (cond.rhs_val > mi) {
                            mi = cond.rhs_val;
                        }
                    } else if (cond.op == OP_LT || cond.op == OP_LE) {
                        if (cond.rhs_val < ma) {
                            ma = cond.rhs_val;
                        }
                    }

                    // std::cout<<"=================================="<<std::endl;
                    // std::cout<<cond.rhs_val.int_val<<std::endl;
                    // std::cout<<ma.int_val<<std::endl;
                    // std::cout<<mi.int_val<<std::endl;
                    // std::cout<<"=================================="<<std::endl;
                }


                
            }

            assert(mi <= ma);
            memcpy(low_k.data + single_of, mi.raw->data, col.len);
            memcpy(up_k.data + single_of, ma.raw->data, col.len);
            single_of += col.len;

        }

        auto low_id = ih_->lower_bound(low_k.data);
        auto up_id = ih_->upper_bound(up_k.data);
        scan_ = std::make_unique<IxScan>(ih_,low_id,up_id,sm_manager_->get_bpm());

        while (!scan_->is_end()){

            //std::cout<<"进入while循环"<<std::endl;
            rid_ = scan_->rid();
            std::unique_ptr<RmRecord> rec = fh_->get_record(rid_, context_);
            int i = 0;
            // for(auto fond_it = fed_conds_.begin(); fond_it != fed_conds_.end();fond_it++) {
            for(int k = 0;k<fed_conds_.size();k++){    

                if(breakCond(fed_conds_[k],cols_,rec)){
                    i++;
                    break;
                }

            }
            if(i==0){
                return;
            }else{
                scan_->next();
            }
        }

    }

    void nextTuple() override {

        // n->check_runtime_conds();

        assert(!is_end());

        scan_->next();

        while (!scan_->is_end()){
            rid_ = scan_->rid();
            std::unique_ptr<RmRecord> rec = fh_->get_record(rid_, context_);
            int i = 0;
            // for(auto fond_it = fed_conds_.begin(); fond_it != fed_conds_.end();fond_it++) {
            for(int k = 0;k<fed_conds_.size();k++){    

                if(breakCond(fed_conds_[k],cols_,rec)){
                    i++;
                    break;
                }

            }
            if(i==0){
                return;
            }else{
                scan_->next();
            }
        }
    }



    bool is_end() const override { return scan_->is_end(); }

    size_t tupleLen() const override { return len_; }

    const std::vector<ColMeta> &cols() const override { return cols_; }

    ColMeta get_col_offset(const TabCol &target) override {
        auto pos = std::find_if(cols_.begin(), cols_.end(), [&](const ColMeta &col) {
            return col.tab_name == target.tab_name && col.name == target.col_name;
        });
        if (pos == cols_.end()) {
            ColMeta col = {.tab_name = "",
               .name = "",
               .type = TYPE_INT,
               .len = -1,
               .offset = -1,
               .index = false};
            return col;
        }
        return *pos;
    }

    std::unique_ptr<RmRecord> Next() override {
        assert(!is_end());
        return fh_->get_record(rid_, context_);
    }


    bool breakCond(Condition fond,std::vector<ColMeta> cols_,const std::unique_ptr<RmRecord> &rec){
            
            auto col = *get_col(cols_, fond.lhs_col);
            Value val;
            val.type = col.type;
            char *data = rec->data + col.offset;
            if(col.type==TYPE_INT){
                int to;
                std::copy(reinterpret_cast<const char*>(data), reinterpret_cast<const char*>(data) + col.len, reinterpret_cast<char*>(&to));
                val.set_int(to);
            }else if(col.type==TYPE_FLOAT){
                float to;
                std::copy(reinterpret_cast<const char*>(data), reinterpret_cast<const char*>(data) + col.len, reinterpret_cast<char*>(&to));
                val.set_float(to);
            }else if(col.type==TYPE_STRING){
                val.set_str(std::string(data, col.len));
            }
            val.init_raw(col.len);
            
            bool com = true;
            if(fond.op==OP_EQ){
                com = val == fond.rhs_val ? true : false; 
            }else if(fond.op==OP_NE){
                com = val != fond.rhs_val ? true:false; 
            }else if(fond.op==OP_LT){
                com = val < fond.rhs_val ? true:false; 
            }else if(fond.op==OP_GT){
                com = val > fond.rhs_val ? true:false; 
            }else if(fond.op==OP_LE){
                com = val <= fond.rhs_val ? true:false; 
            }else if(fond.op==OP_GE){
                com = val >= fond.rhs_val ? true:false; 
            }

            return (fond.is_rhs_val&&!com);
    }

    std::pair<Value,Value> get_Max_Min(ColMeta col){


        Value ma;
        Value mi;
        if(col.type==TYPE_INT){
            ma.set_int(INT32_MAX);
            mi.set_int(INT32_MIN);
            ma.init_raw(col.len);
            mi.init_raw(col.len);
        }else if(col.type==TYPE_FLOAT){
            ma.set_float(__FLT_MAX__);
            mi.set_float(-__FLT_MAX__);
            ma.init_raw(col.len);
            mi.init_raw(col.len);
        }else if(col.type==TYPE_STRING){
            ma.set_str(std::string(col.len, 255));
            mi.set_str(std::string(col.len, 0));
            ma.init_raw(col.len);
            mi.init_raw(col.len);
        }else if(col.type==TYPE_BIGINT){
            ma.set_bigint(INT64_MAX);
            mi.set_bigint(INT64_MIN);
            ma.init_raw(col.len);
            mi.init_raw(col.len);
        }

        return {ma,mi};

    }


    Rid &rid() override { return rid_; }
};