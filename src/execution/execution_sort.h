/* Copyright (c) 2023 Renmin University of China
RMDB is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

#pragma once
#include <numeric>
#include "execution_defs.h"
#include "execution_manager.h"
#include "executor_abstract.h"
#include "index/ix.h"
#include "system/sm.h"

class SortExecutor : public AbstractExecutor {
   private:
    std::unique_ptr<AbstractExecutor> prev_;
    // ColMeta cols_;                              // 框架中只支持一个键排序，需要自行修改数据结构支持多个键排序
    std::vector<ColMeta> sort_keys_;               // 07、15 修改sort支持多个排序条件
    // 07/18 修改is_desc为数组，因为存在每个列单独升序降序
    std::vector<bool> is_desc_;  // 是否降序 T为降 F为升
    size_t tuple_num;
    
    // std::vector<size_t> used_tuple;
    RmRecord current_tuple;

    std::vector<std::unique_ptr<RmRecord>> records;
    int limit_ = -1; // 新增limit 默认为-1,不能为0
    int count_ = 0;
    

  public:
    SortExecutor(std::unique_ptr<AbstractExecutor> prev, std::vector<TabCol> sel_cols, std::vector<bool> is_desc,int limit) {
        
        
        
        prev_ = std::move(prev);


       // 获取多个排序键的元数据信息
        for(auto &col : sel_cols){
            // std::cout<<col.col_name<<col.tab_name<<"数据量"<<sel_cols.size()<<std::endl;
            sort_keys_.push_back(prev_->get_col_offset(col));
        }
        is_desc_ = is_desc;

        tuple_num = 0;
        // used_tuple.clear();
        limit_ = limit;

    }
    std::string getType() override { return "Sort"; }

    size_t tupleLen() const override { return prev_->tupleLen(); }

    const std::vector<ColMeta> &cols() const override { return prev_->cols(); }

// 07/17  完成sort算子的补充
void beginTuple() override {

    // 以下为遍历排序，第八题超时，已废

        // used_tuple.clear();
    // bool start = true;
    // char *current_date;
    // size_t cmp_index = 0, curr_index = 0;

    // for (prev_->beginTuple(); !prev_->is_end(); prev_->nextTuple()) {
    //     auto Tuple = prev_->Next();
    //     // 这个i用来遍历is_isdesc
    //     size_t i = 0;

    //     if (start) {// 初始化
    //         curr_index = cmp_index;
    //         start = false;
    //         current_tuple = std::move(Tuple);
    //     } else {
    //         for (const auto& col : sort_keys_) {
    //             char* rec_buf = Tuple->data + col.offset;
    //             int cmp = ix_compare(current_tuple->data + col.offset, rec_buf, col.type, col.len);

    //             if ((is_desc_[i] && cmp < 0) || (!is_desc_[i] && cmp > 0)) {
    //                 curr_index = cmp_index;
    //                 current_tuple = std::move(Tuple);
    //                 break;
    //             } else if (cmp == 0) {
    //                 ++i;
    //                 if (i < sort_keys_.size()) {
    //                     // 更新 current_date 的值
    //                     current_date = current_tuple->data + sort_keys_[i].offset;
    //                     rec_buf = Tuple->data + sort_keys_[i].offset;
    //                     continue;
    //                 }
    //             }
    //             break;
    //         }
    //     }

    //     tuple_num++;
    //     cmp_index++;
    // }

    // used_tuple.emplace_back(curr_index);

    // 


    // std::cout<<111111<<std::endl;
    // used_tuple.clear();
    for( prev_->beginTuple(); !prev_->is_end(); prev_->nextTuple()){
     

            // 获取所有记录
            // auto record = prev_ -> Next();
            records.push_back(std::move(prev_ -> Next()));
        }
    // std::cout<<22222222<<std::endl;
    // lambda比较
        std::sort(records.begin(), records.end(), [this](const std::unique_ptr<RmRecord> &lhs, const std::unique_ptr<RmRecord> &rhs) {
            return this->compare(lhs, rhs);
        });
    // std::cout<<3333333<<std::endl;
    tuple_num = 0;

}


void nextTuple() override {

        // bool start = true;
    // size_t cmp_index = 0, curr_index = 0;

    // for (prev_->beginTuple(); !prev_->is_end(); prev_->nextTuple()) {
    //     if (std::find(used_tuple.begin(), used_tuple.end(), cmp_index) != used_tuple.end()) {
    //         cmp_index++;
    //         continue;
    //     }
        
    //     auto Tuple = prev_->Next();
    //     char* rec_buf = Tuple->data + sort_keys_[0].offset;

    //     if (start) {
    //         curr_index = cmp_index;
    //         start = false;
    //         current_tuple = std::move(Tuple);
    //     } else {
    //         size_t i = 0;
    //         char* current_date = current_tuple->data + sort_keys_[i].offset;
            
    //         for (const auto& col : sort_keys_) {
    //             int cmp = ix_compare(current_date, rec_buf, col.type, col.len);

    //             if ((is_desc_[i] && cmp < 0) || (!is_desc_[i] && cmp > 0)) {
    //                 curr_index = cmp_index;
    //                 current_tuple = std::move(Tuple);
    //                 break;
    //             } else if (cmp == 0) {
    //                 ++i;
    //                 if (i < sort_keys_.size()) {
    //                     // 更新 current_date 的值
    //                     current_date = current_tuple->data + sort_keys_[i].offset;
    //                     rec_buf = Tuple->data + sort_keys_[i].offset;
    //                     continue;
    //                 }
    //             }
    //             break;
    //         }
    //     }
        
    //     cmp_index++;
    // }

    // tuple_num--;
    // used_tuple.emplace_back(curr_index);

    // prev_->nextTuple();
    
    tuple_num++;
        
}

bool compare(const std::unique_ptr<RmRecord> &lhs, const std::unique_ptr<RmRecord> &rhs){
    // 传入左右两记录
    int i = 0;
    for(auto col : sort_keys_){
        // 比较字段
        int cmp = ix_compare(lhs->data + col.offset, rhs->data + col.offset ,col.type,col.len);

        if((cmp < 0 && !is_desc_[i]) || (cmp > 0 && is_desc_[i])){
            return true;
        }else if(cmp == 0){
            i++;
            continue;
            
        }else return false;
    }
    return false;

}

    bool is_end() const override { 

        if(limit_ != -1 && tuple_num >= limit_) {
            return true;
        }
        return tuple_num >= records.size(); 
        }

    std::unique_ptr<RmRecord> Next() override {
        return std::move(records[tuple_num]);
    }
    
    // ColMeta get_col_offset(const TabCol &target) override {
    //     return prev_->get_col_offset(target);
    // }

    Rid &rid() override { return _abstract_rid; }

    
};