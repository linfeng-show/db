#pragma once
#include "execution_defs.h"
#include "execution_manager.h"
#include "executor_abstract.h"
#include "index/ix.h"
#include "system/sm.h"

// ??????????????
class Insert_Mu_Executor : public AbstractExecutor {
   private:
    TabMeta tab_; // ?????????
    std::vector<std::vector<Value>> va;      // ??????????????          
    std::vector<Value> values_;     // ??????????????
    RmFileHandle *fh_;              // ??????????????
    std::string tab_name_;          // ??????
    Rid rid_;                       // ?????��?????????????????????��????????rid_?????????
    SmManager *sm_manager_;

   public:
    Insert_Mu_Executor(SmManager *sm_manager, const std::string &tab_name, std::vector<std::vector<Value>> values, Context *context) {
        sm_manager_ = sm_manager;
        tab_ = sm_manager_->db_.get_table(tab_name);
        va = values;
        tab_name_ = tab_name;
        if (va[0].size() != tab_.cols.size()) {
            throw InvalidValueCountError();
        }
        fh_ = sm_manager_->fhs_.at(tab_name).get();
        context_ = context;
    
    };

    std::unique_ptr<RmRecord> Next() override {

        // std::cout<<"insert???"<<std::endl;
        // Make record buffer
        RmRecord rec(fh_->get_file_hdr().record_size);

        for(size_t t = 0;t < va.size();t++){
            values_ = va[t];
            for (size_t i = 0; i < values_.size(); i++) {

            // std::cout<<"??????��??? ????????"<<values_.size()<<std::endl;

            auto &col = tab_.cols[i];
            auto &val = values_[i];
            // if (col.type != val.type) {
            //     // ?????????int -?? bigint
            //     if(val.type == TYPE_INT && col.type == TYPE_BIGINT){
            //         val.type = TYPE_BIGINT;
            //         val.bigint_val = val.int_val;
            //     }

            //     // ??????????date????char??????????
            //     if(val.type == TYPE_DATETIME && col.type == TYPE_STRING){
            //         val.type = TYPE_STRING;
            //         val.str_val = val.datetime.ToString();
            //     }
            //     if (col.type != val.type){
            //          throw IncompatibleTypeError(coltype2str(col.type), coltype2str(val.type));
            //     }
               
            // }
            val.init_raw(col.len);
            memcpy(rec.data + col.offset, val.raw->data, col.len);
        }
        rid_ = fh_->insert_record_nolock(rec.data, context_);

        // ???load??��????��????????
        // if(sm_manager_->whether_write_record){
        //     InsertLogRecord* insert_log = new InsertLogRecord(context_->txn_->get_transaction_id(),context_->txn_->get_prev_lsn(),
        //             rec, rid_, tab_name_);
        //     insert_log->prev_lsn_ = context_->txn_->get_prev_lsn();

        //     context_->log_mgr_->add_log_to_buffer(insert_log);
        //     context_->txn_->set_prev_lsn(insert_log->lsn_);

        //     WriteRecord *write_record = new WriteRecord(WType::INSERT_TUPLE, tab_name_, rid_);
        //     context_->txn_->append_write_record(write_record);
        // }

        // ???????

        // Insert into index
        try{
            for(size_t i = 0; i < tab_.indexes.size(); ++i) {
                auto& index = tab_.indexes[i];
                auto ih = sm_manager_->ihs_.at(sm_manager_->get_ix_manager()->get_index_name(tab_name_, index.cols)).get();
                char* key = new char[index.col_tot_len];
                int offset = 0;
                for(size_t i = 0; i < index.col_num; ++i) {
                    memcpy(key + offset, rec.data + index.cols[i].offset, index.cols[i].len);
                    offset += index.cols[i].len;
                }
                ih->insert_entry(key, rid_, context_->txn_);
                delete[] key;
            }
        }catch(InternalError &err){
            fh_->delete_record(rid_, context_);
            throw InternalError("Not unique index");
        }
        }
        return nullptr;
    }
    Rid &rid() override { return rid_; }
};