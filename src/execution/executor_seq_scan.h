/* Copyright (c) 2023 Renmin University of China
RMDB is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

#pragma once

#include "execution_defs.h"
#include "execution_manager.h"
#include "executor_abstract.h"
#include "index/ix.h"
#include "system/sm.h"

class SeqScanExecutor : public AbstractExecutor {
   private:
    std::string tab_name_;              // 表的名称
    std::vector<Condition> conds_;      // scan的条件
    RmFileHandle *fh_;                  // 表的数据文件句柄
    std::vector<ColMeta> cols_;         // scan后生成的记录的字段
    size_t len_;                        // scan后生成的每条记录的长度
    std::vector<Condition> fed_conds_;  // 同conds_，两个字段相同

    Rid rid_;
    std::unique_ptr<RecScan> scan_;     // table_iterator

    SmManager *sm_manager_;

   public:
    SeqScanExecutor(SmManager *sm_manager, std::string tab_name, std::vector<Condition> conds, Context *context) {
        sm_manager_ = sm_manager;
        tab_name_ = std::move(tab_name);
        conds_ = std::move(conds);
        TabMeta &tab = sm_manager_->db_.get_table(tab_name_);
        fh_ = sm_manager_->fhs_.at(tab_name_).get();
        cols_ = tab.cols;
        len_ = cols_.back().offset + cols_.back().len;

        context_ = context;

        fed_conds_ = conds_;
        for (auto &cond : fed_conds_) {
            assert(cond.lhs_col.tab_name == tab_name_);
            if (!cond.is_rhs_val) {
                assert(cond.rhs_col.tab_name == tab_name_);
            }
        }

        // 07/26 搜索算子共享锁
        if(context != nullptr) {
            context->lock_mgr_->lock_shared_on_table(context->txn_, fh_->GetFd());
        }

    }

    std::string getType() override { return "SeqScan"; }
    void beginTuple() override {
                //check_runtime_conds();

        scan_ = std::make_unique<RmScan>(fh_);

        // Get the first record
        while (!scan_->is_end()) {
           
            rid_ = scan_->rid();
            if(fed_conds_.empty())
                return;
            auto rec = fh_->get_record(rid_, context_);  // TableHeap->GetTuple()
            
            if (eval_conds(cols_, fed_conds_, rec.get())) {
                break;
            }
            scan_->next();  // 找下一个有record的位置
        }
    }

    void nextTuple() override {
                //check_runtime_conds();
        assert(!is_end());
        for (scan_->next(); !scan_->is_end(); scan_->next()) {  // 用TableIterator遍历TableHeap中的所有Tuple
           
           rid_ = scan_->rid();
           if(fed_conds_.empty())
            return;
            auto rec = fh_->get_record(rid_, context_);
            if (eval_conds(cols_, fed_conds_, rec.get())) {  // Tuple是否满足Schema模式
                break;
            }
        }
    }

    bool is_end() const override { return scan_->is_end(); }

    size_t tupleLen() const override { return len_; }

    const std::vector<ColMeta> &cols() const override { return cols_; }

    std::unique_ptr<RmRecord> Next() override {
        assert(!is_end());
        return fh_->get_record(rid_, context_);
    }

    Rid &rid() override { return rid_; }

    ColMeta get_col_offset(const TabCol &target) override {
        auto pos = std::find_if(cols_.begin(), cols_.end(), [&](const ColMeta &col) {
            return col.tab_name == target.tab_name && col.name == target.col_name;
        });
        if (pos == cols_.end()) {
            ColMeta col = {.tab_name = "",
               .name = "",
               .type = TYPE_INT,
               .len = -1,
               .offset = -1,
               .index = false};
            return col;
        }
        return *pos;
    }

    // void check_runtime_conds() {
    //     for (auto &cond : fed_conds_) {
    //         assert(cond.lhs_col.tab_name == tab_name_);
    //         if (!cond.is_rhs_val) {
    //             assert(cond.rhs_col.tab_name == tab_name_);
    //         }
    //     }
    // }

    bool eval_cond(const std::vector<ColMeta> &rec_cols, const Condition &cond, const RmRecord *rec) {
        auto lhs_col = get_col(rec_cols, cond.lhs_col);
        char *lhs = rec->data + lhs_col->offset;
        char *rhs;
        ColType rhs_type;
        
        if (cond.is_rhs_val) {
            rhs_type = cond.rhs_val.type;
            rhs = cond.rhs_val.raw->data;

        } else {
            // rhs is a column
            auto rhs_col = get_col(rec_cols, cond.rhs_col);
            rhs_type = rhs_col->type;
            rhs = rec->data + rhs_col->offset;
        }
       // assert(rhs_type == lhs_col->type);  // TODO convert to common type

        // std::cout<<"doing comp nowing~~~"<<std::endl;

        int cmp = 0;

        if(rhs_type == TYPE_FLOAT && lhs_col->type == TYPE_INT){
            int i = *(int *)lhs;
            float lhs_float = static_cast<float>(i); 
            char buf[sizeof(float)]; 
            memcpy(buf, &lhs_float, sizeof(float));
            cmp = ix_compare(buf, rhs, rhs_type, lhs_col->len);
        }else{
            cmp = ix_compare(lhs, rhs, rhs_type, lhs_col->len);
        }

        // std::cout<<"cond.op:"<<cond.op<<std::endl;
        // std::cout<<"cmp: "<<cmp<<std::endl;

        if (cond.op == OP_EQ) {
            return cmp == 0;
        } else if (cond.op == OP_NE) {
            return cmp != 0;
        } else if (cond.op == OP_LT) {
            return cmp < 0;
        } else if (cond.op == OP_GT) {
            return cmp > 0;
        } else if (cond.op == OP_LE) {
            return cmp <= 0;
        } else if (cond.op == OP_GE) {
            return cmp >= 0;
        } else {
            throw InternalError("Unexpected op type");
        }
    }

    bool eval_conds(const std::vector<ColMeta> &rec_cols, const std::vector<Condition> &conds, const RmRecord *rec) {
        return std::all_of(conds.begin(), conds.end(),
                           [&](const Condition &cond) { return eval_cond(rec_cols, cond, rec); });
    }

};
