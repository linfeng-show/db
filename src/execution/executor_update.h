/* Copyright (c) 2023 Renmin University of China
RMDB is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

#pragma once
#include "execution_defs.h"
#include "execution_manager.h"
#include "executor_abstract.h"
#include "index/ix.h"
#include "system/sm.h"

class UpdateExecutor : public AbstractExecutor {
   private:
    TabMeta tab_;
    std::vector<Condition> conds_;
    RmFileHandle *fh_;
    std::vector<Rid> rids_;
    std::string tab_name_;
    std::vector<SetClause> set_clauses_;
    SmManager *sm_manager_;

   public:
    UpdateExecutor(SmManager *sm_manager, const std::string &tab_name, std::vector<SetClause> set_clauses,
                   std::vector<Condition> conds, std::vector<Rid> rids, Context *context) {
        sm_manager_ = sm_manager;
        tab_name_ = tab_name;
        set_clauses_ = set_clauses;
        tab_ = sm_manager_->db_.get_table(tab_name);
        fh_ = sm_manager_->fhs_.at(tab_name).get();
        conds_ = conds;
        rids_ = rids;
        context_ = context;

        // 07/26 更新算子排它锁
        if(context_ != nullptr) {
            context_->lock_mgr_->lock_IX_on_table(context_->txn_, fh_->GetFd());
        }

    }
    std::unique_ptr<RmRecord> Next() override {
        

        //std::cout<<"update执行"<<std::endl;
       // std::cout<<tab_.indexes.size()<<std::endl;

                
        for (auto &rid : rids_) {

            auto rec = fh_->get_record(rid, context_);
            
            // 备份原始
            char old_rec[fh_->get_file_hdr().record_size];
            memcpy(old_rec, rec->data, fh_->get_file_hdr().record_size);


            // 删除索引
            for(size_t i = 0; i < tab_.indexes.size(); ++i) {
                auto& index = tab_.indexes[i];
                auto ih = sm_manager_->ihs_.at(sm_manager_->get_ix_manager()->get_index_name(tab_name_, index.cols)).get();
                char* key = new char[index.col_tot_len];
                int offset = 0;
                for(size_t i = 0; i < index.col_num; i++) {
                    memcpy(key + offset, rec->data + index.cols[i].offset, index.cols[i].len);
                    offset += index.cols[i].len;
                }
                ih->delete_entry(key,context_->txn_);
            }
            
            RmRecord update_record{rec->size};
            memcpy(update_record.data, rec->data, rec->size);

            char new_rec[fh_->get_file_hdr().record_size];
            memcpy(new_rec, rec->data, fh_->get_file_hdr().record_size);


            for(auto &set_clause: set_clauses_) {
                auto lhs_col = tab_.get_col(set_clause.lhs.col_name);
                if(set_clause.cpy){
                    Value val_date;
                    char col_val[lhs_col->len];
                    memcpy(col_val, rec->data+ lhs_col->offset, lhs_col->len);
                   // std::cout<<'\n'<<col_val<<'\n';
                    if(lhs_col->type==TYPE_FLOAT){
                    float Tcol_val=*(float*)col_val;
                    if(set_clause.si==1)
                    Tcol_val+=*(float*)set_clause.rhs.raw->data;
                    else if(set_clause.si==2)
                    Tcol_val-=*(float*)set_clause.rhs.raw->data;
                    else
                    Tcol_val=*(float*)set_clause.rhs.raw->data - Tcol_val;
                    val_date.set_float(Tcol_val);
                    val_date.init_raw(sizeof(float));
                    }
                    else if(lhs_col->type==TYPE_INT){
                    int Tcol_val=*(int*)col_val;
                    //std ::cout<<Tcol_val;
                    if(set_clause.si==1)
                    Tcol_val+=*(int*)set_clause.rhs.raw->data;
                    else if(set_clause.si==2)
                    Tcol_val-=*(int*)set_clause.rhs.raw->data;
                    else
                    Tcol_val=*(int*)set_clause.rhs.raw->data-Tcol_val;
                    val_date.set_int(Tcol_val);
                    val_date.init_raw(sizeof(int));
                    }
                    else if(lhs_col->type==TYPE_BIGINT){
                    int64_t Tcol_val=*(int64_t*)col_val;
                    if(set_clause.si==1)
                    Tcol_val+=*(int64_t*)set_clause.rhs.raw->data;
                    else if(set_clause.si==2)
                    Tcol_val-=*(int64_t*)set_clause.rhs.raw->data;
                    else
                    Tcol_val=*(int64_t*)set_clause.rhs.raw->data-Tcol_val;
                    val_date.set_bigint(Tcol_val);
                    val_date.init_raw(sizeof(int64_t));
                    }
                    else{
                        throw InternalError(lhs_col->name+" not can + or - ");
                    }
                    //char *p=(char*)val_date.c_str();
                    //std::strcpy(col_val,val_date.c_str());
                   // std ::cout<<'\n'<<col_val<<'\n';
                    memcpy(new_rec + lhs_col->offset, val_date.raw->data, lhs_col->len);
                   // std ::cout<<'\n'<<lhs_col->offset<<'\n'<<new_rec + lhs_col->offset<<'\n'<<*(int*)(new_rec + lhs_col->offset)<<'\n';
                   // std::cout<<set_clause.rhs.raw->data<<'\n';
                }
                else
                memcpy(new_rec + lhs_col->offset, set_clause.rhs.raw->data, lhs_col->len);
            }


            // 更新
            fh_->update_record(rid, new_rec, context_);

            // 构造数据
            RmRecord new_record{rec->size};
            memcpy(new_record.data,rec->data,rec->size);

            
            if(sm_manager_->whether_write_record){
                // DeleteLogRecord* delete_log = new DeleteLogRecord(context_->txn_->get_transaction_id(),context_->txn_->get_prev_lsn(),
                //         update_record, rid, tab_name_);
                std::unique_ptr<DeleteLogRecord> delete_log(new DeleteLogRecord(context_->txn_->get_transaction_id(),context_->txn_->get_prev_lsn(),
                    update_record, rid, tab_name_));

                delete_log->prev_lsn_ = context_->txn_->get_prev_lsn();
                context_->log_mgr_->add_log_to_buffer(delete_log.get());
                context_->txn_->set_prev_lsn(delete_log->lsn_);

                WriteRecord *write_record = new WriteRecord(WType::DELETE_TUPLE, tab_name_, rid, update_record);
                context_->txn_->append_write_record(write_record);
                
                
                // InsertLogRecord* insert_log = new InsertLogRecord(context_->txn_->get_transaction_id(),context_->txn_->get_prev_lsn(),
                //         new_record, rid, tab_name_);
                std::unique_ptr<InsertLogRecord> insert_log(new InsertLogRecord(context_->txn_->get_transaction_id(),context_->txn_->get_prev_lsn(),
                        new_record, rid, tab_name_));

                insert_log->prev_lsn_ = context_->txn_->get_prev_lsn();

                context_->log_mgr_->add_log_to_buffer(insert_log.get());
                context_->txn_->set_prev_lsn(insert_log->lsn_);

                write_record = new WriteRecord(WType::INSERT_TUPLE, tab_name_, rid);
                context_->txn_->append_write_record(write_record);
            }


            try{
                // 插入
                for(size_t i = 0; i < tab_.indexes.size(); ++i) {
                    auto& index = tab_.indexes[i];
                    auto ih = sm_manager_->ihs_.at(sm_manager_->get_ix_manager()->get_index_name(tab_name_, index.cols)).get();
                    char* key = new char[index.col_tot_len];
                    int offset = 0;
                    for(size_t i = 0; i < index.col_num; i++) {
                        memcpy(key + offset, new_rec + index.cols[i].offset, index.cols[i].len);
                        offset += index.cols[i].len;
                    }
                    ih->insert_entry(key,rid,context_->txn_);
                }
            }catch(InternalError &err){
                
                // 插入失败，恢复
                fh_->update_record(rid,old_rec,context_);
                
                for(size_t i = 0; i < tab_.indexes.size(); ++i) {
                    auto& index = tab_.indexes[i];
                    auto ih = sm_manager_->ihs_.at(sm_manager_->get_ix_manager()->get_index_name(tab_name_, index.cols)).get();
                    char* key = new char[index.col_tot_len];
                    int offset = 0;
                    for(size_t i = 0; i < index.col_num; i++) {
                        memcpy(key + offset, old_rec + index.cols[i].offset, index.cols[i].len);
                        offset += index.cols[i].len;
                    }
                    ih->insert_entry(key,rid,context_->txn_);
                }
                throw InternalError("Not unique index");
            }
         }
        return nullptr;
    }

    Rid &rid() override { return _abstract_rid; }
};
