/* Copyright (c) 2023 Renmin University of China
RMDB is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

#pragma once
#include "execution_defs.h"
#include "execution_manager.h"
#include "executor_abstract.h"
#include "index/ix.h"
#include "system/sm.h"
#include "common/join_buffer.h"

class NestedLoopJoinExecutor : public AbstractExecutor {
   private:
    std::unique_ptr<AbstractExecutor> left_;    // 左儿子节点（需要join的表）
    std::unique_ptr<AbstractExecutor> right_;   // 右儿子节点（需要join的表）
    size_t len_;                                // join后获得的每条记录的长度
    std::vector<ColMeta> cols_;                 // join后获得的记录的字段
    std::vector<Condition> fed_conds_;          // join条件
    bool isend;

    
    // int currentLeftBlock_ = 0;  // 当前左表块的索引
    // int currentRightBlock_ = 0;  // 当前右表块的索引
    // 07/19 利用joinbuffer，实现块嵌套
    JoinBuffer left_joinBuffer_;
    JoinBuffer right_joinBuffer_;

    // std::vector<JoinBuffer> leftBlock_;  // 左表块中的记录
    // std::vector<JoinBuffer> rightBlock_;  // 右表块中的记录
   public:
    NestedLoopJoinExecutor(std::unique_ptr<AbstractExecutor> left, std::unique_ptr<AbstractExecutor> right,
                           std::vector<Condition> conds) {
        // std::cout<<"join执行器"<<std::endl;
        left_ = std::move(left);
        right_ = std::move(right);
        len_ = left_->tupleLen() + right_->tupleLen();
        cols_ = left_->cols();
        auto right_cols = right_->cols();
        for (auto& col : right_cols) {
            col.offset += left_->tupleLen();
        }
        cols_.insert(cols_.end(), right_cols.begin(), right_cols.end());
        isend = false;
        fed_conds_ = std::move(conds);
    }
    std::string getType() override { return "Join"; }
    size_t tupleLen() const override { return len_; }
    const std::vector<ColMeta>& cols() const override { return cols_; }

    void beginTuple() override {
        // 左右连接池和块进行初始化
        left_->beginTuple();
        left_joinBuffer_.reset();
        while(!left_->is_end() && !left_joinBuffer_.isFull()){
            left_joinBuffer_.push(std::move(left_->Next()));
            left_->nextTuple();
        }

        right_->beginTuple();
        right_joinBuffer_.reset();
        while(!right_->is_end() && !right_joinBuffer_.isFull()){
            right_joinBuffer_.push(std::move(right_->Next()));
            right_->nextTuple();
        }

        // 初始化结束
        // std::cout<<"初始化结束"<<std::endl;
        // 开始将两个表的首块送入缓冲池
        // 块索引置0
        // currentLeftBlock_ = 0;
        // currentRightBlock_ = 0;        
        // 池索引置0
        left_joinBuffer_.pos = 0;
        right_joinBuffer_.pos = 0;

        
        isend = false;
        // 开始循环查找
        // std::cout<<"开始循环查找"<<std::endl;
        find_next_record();
    }
    void nextTuple() override {
        // std::cout<<"进入nextT"<<std::endl;
        // 加断言，防止溢出bug
       assert(!is_end());
       right_joinBuffer_.pos++;
    //    std::cout<<"找下一条"<<std::endl;
       find_next_record();
    }
    bool is_end() const override { return isend; }
    Rid &rid() override { return _abstract_rid; }

    std::unique_ptr<RmRecord> Next() override {
        // std::cout<<"开始获取输出"<<std::endl;
    // 加断言，防止溢出bug
       assert(!is_end());
    //    将符合条件的记录连在一起
        auto result = std::make_unique<RmRecord>(len_);
        auto left = left_joinBuffer_.getRecord();
        auto right = right_joinBuffer_.getRecord();
        memcpy(result->data, left->data,left->size);
        memcpy(result->data + left->size, right->data, right->size);
        // std::cout<<"组转成功"<<std::endl;
        return result;
    }

    void find_next_record() {
        while(left_joinBuffer_.len != 0){
            while(right_joinBuffer_.len != 0){
                while(!left_joinBuffer_.isEnd()){
                    // 从左池中取出一条记录，和右池的所有记录比较
                    auto left = left_joinBuffer_.getRecord();
                    while(!right_joinBuffer_.isEnd()){
                        // 遍历右池所有的记录
                        auto right = right_joinBuffer_.getRecord();
                        
                        bool yes = true;
                        for (const auto& cond : fed_conds_) {
                            if (!eval_cond(cond, left, right)) {
                                // 存在一个条件不符合就可以break掉了
                                yes = false;
                                break;
                        }
                        }
                        // 条件符合情况
                        if(yes){
                            // std::cout<<"条件符合"<<std::endl;
                            return;
                        }
                        // break掉的情况
                        // 右池索引 加1
                        right_joinBuffer_.pos++;
                    }
                    // 左池索引 加1，新取出的数据再次和右池所有数据比较
                    left_joinBuffer_.pos++;
                    // 右池索引重新置0
                    right_joinBuffer_.pos = 0;
                }
                // 左池结束 索引置0 准备再次遍历
                // 右池填充新块
                left_joinBuffer_.pos = 0;
                // currentRightBlock_++; // 右块索引后移
                // right_joinBuffer_ = rightBlock_[currentRightBlock_];
                // std::swap(right_joinBuffer_,rightBlock_[currentRightBlock_]);
                Load_Next_Right();
            }
            // 右块结束 块索引置0 准备再次遍历
            // 右池也要重新填充
            // 左池填充新块
            // currentLeftBlock_++;
            // left_joinBuffer_ = leftBlock_[currentLeftBlock_];
            // std::swap(left_joinBuffer_,leftBlock_[currentLeftBlock_]);
            Load_Next_Left();
            // currentRightBlock_ = 0;
            // right_joinBuffer_ = rightBlock_[currentRightBlock_];
            // std::swap(right_joinBuffer_,rightBlock_[currentRightBlock_]);
            right_->beginTuple();
            right_joinBuffer_.reset();
            while(!right_->is_end() && !right_joinBuffer_.isFull()){
                right_joinBuffer_.push(std::move(right_->Next()));
                right_->nextTuple();
            }

        }
        isend = true;
        // std::cout<<"查找结束"<<std::endl;
        return;
    }




    ColMeta get_col_offset(const TabCol& target) override {
        auto left_pos = left_->get_col_offset(target);
        if (left_pos.len == -1) {
            auto right_pos = right_->get_col_offset(target);
            ColMeta col = {
                .tab_name = right_pos.tab_name,
                .name = right_pos.name,
                .type = right_pos.type,
                .len = right_pos.len,
                .offset = right_pos.offset + static_cast<int>(left_->tupleLen()),
                .index = right_pos.index
            };
            return col;
        }
        return left_pos;
    }
    bool eval_cond(const Condition& cond, const RmRecord* left, const RmRecord* right) {
        
        auto lhs_col = left_->get_col_offset(cond.lhs_col);
        char* lhs = (char*)left->data + lhs_col.offset;
        auto rhs_col = right_->get_col_offset(cond.rhs_col);
        char* rhs = (char*)right->data + rhs_col.offset;
        assert(rhs_col.type == lhs_col.type);
        int cmp = ix_compare(lhs, rhs, rhs_col.type, rhs_col.len);
        if (cond.op == OP_EQ) {
            return cmp == 0;
        } else if (cond.op == OP_NE) {
            return cmp != 0;
        } else if (cond.op == OP_LT) {
            return cmp < 0;
        } else if (cond.op == OP_GT) {
            return cmp > 0;
        } else if (cond.op == OP_LE) {
            return cmp <= 0;
        } else if (cond.op == OP_GE) {
            return cmp >= 0;
        } else {
            throw InternalError("can't match condition");
        }
    }
    bool eval_conds(const std::vector<Condition>& conds, const RmRecord* left, const RmRecord* right) {
        return std::all_of(conds.begin(), conds.end(),
                           [&](const Condition& cond) { return eval_cond(cond, left, right); });
    }

    void Load_Next_Left(){
        assert(!is_end());
        left_joinBuffer_.reset();
        while(!left_->is_end() && !left_joinBuffer_.isFull()){
            left_joinBuffer_.push(std::move(left_->Next()));
            left_->nextTuple();
        }
    }

    void Load_Next_Right(){
        assert(!is_end());
        right_joinBuffer_.reset();
        while(!right_->is_end() && !right_joinBuffer_.isFull()){
            right_joinBuffer_.push(std::move(right_->Next()));
            right_->nextTuple();
        }
    }

};

