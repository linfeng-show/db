/* Copyright (c) 2023 Renmin University of China
RMDB is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

#include "rm_file_handle.h"

/**
 * @description: 获取当前表中记录号为rid的记录
 * @param {Rid&} rid 记录号，指定记录的位置
 * @param {Context*} context
 * @return {unique_ptr<RmRecord>} rid对应的记录对象指针
 */
std::unique_ptr<RmRecord> RmFileHandle::get_record(const Rid& rid, Context* context) const {
    // Todo:
    // 1. 获取指定记录所在的page handle
    // 2. 初始化一个指向RmRecord的指针（赋值其内部的data和size）
    
    // 加锁
    if(context!=nullptr){
        context->lock_mgr_->lock_shared_on_record(context->txn_,rid,fd_);
    }

    RmPageHandle  handle = fetch_page_handle(rid.page_no);

    // 6/29 补充可能出现的异常抛出
    if(rid.page_no > handle.file_hdr->num_pages || rid.slot_no > handle.file_hdr->num_records_per_page){
        throw RecordNotFoundError(rid.page_no,rid.slot_no);
    }

    // if (!Bitmap::is_set(handle.bitmap, rid.slot_no)) {
    //     throw RecordNotFoundError(rid.page_no, rid.slot_no);
    // }
    //以上
    std::unique_ptr<RmRecord> record(new RmRecord());
    record->size = file_hdr_.record_size;
    record->data = handle.get_slot(rid.slot_no);

    // 添加
    // LockDataId lock_da_id = LockDataId{fd_,rid,LockDataType::RECORD};
    // context->txn_->get_lock_set()->insert(lock_da_id);
    buffer_pool_manager_->unpin_page(handle.page->get_page_id(),false);
    
    return record;
}

/**
 * @description: 在当前表中插入一条记录，不指定插入位置
 * @param {char*} buf 要插入的记录的数据
 * @param {Context*} context
 * @return {Rid} 插入的记录的记录号（位置）
 */
Rid RmFileHandle::insert_record(char* buf, Context* context) {
    // Todo:
    // 1. 获取当前未满的page handle
    // 2. 在page handle中找到空闲slot位置
    // 3. 将buf复制a到空闲slot位置
    // 4. 更新page_handle.page_hdr中的数据结构
    // 注意考虑插入一条记录后页面已满的情况，需要更新file_hdr_.first_free_page_no

    
    auto page_handle = create_page_handle();  // 获取新页或者未满页
    // 补充可能出现的异常代码

    // 以上
    // std::cout<<"insert正在插入 数据："<<std::endl;
    // for(int i = 0; i <page_handle.file_hdr->record_size; i++) {
    //     std::cout << buf[i]; 
    // }

    //在page handle中找到空闲slot位置
    int free_slot = Bitmap::first_bit(false, page_handle.bitmap, file_hdr_.num_records_per_page);

    // 加锁
    Rid r{page_handle.page->get_page_id().page_no,free_slot};
    if(context!=nullptr){
        context->lock_mgr_->lock_exclusive_on_record(context->txn_,r,fd_);
    }
    
    //更新数据
    char *record_data = page_handle.get_slot(free_slot);
    memcpy(record_data, buf, page_handle.file_hdr->record_size);

    Bitmap::set(page_handle.bitmap,free_slot);//更新bitmap

    page_handle.page_hdr->num_records++;

    if( page_handle.page_hdr->num_records == file_hdr_.num_records_per_page){//页面已满
    //此处更新下一个空闲页号
        file_hdr_.first_free_page_no = page_handle.page_hdr->next_free_page_no;
    }

    Rid rid = Rid{.page_no =page_handle.page->get_page_id().page_no,.slot_no = free_slot};
     buffer_pool_manager_->unpin_page(page_handle.page->get_page_id(),true);
    return rid;
}

Rid RmFileHandle::insert_record_nolock(char* buf, Context* context) {

    auto page_handle = create_page_handle();  // 获取新页或者未满页
    // 补充可能出现的异常代码

    //在page handle中找到空闲slot位置
    int free_slot = Bitmap::first_bit(false, page_handle.bitmap, file_hdr_.num_records_per_page);
  
    //更新数据
    char *record_data = page_handle.get_slot(free_slot);
    memcpy(record_data, buf, page_handle.file_hdr->record_size);


    
    Bitmap::set(page_handle.bitmap,free_slot);//更新bitmap

    page_handle.page_hdr->num_records++;

    if( page_handle.page_hdr->num_records == file_hdr_.num_records_per_page){//页面已满
    //此处更新下一个空闲页号
        file_hdr_.first_free_page_no = page_handle.page_hdr->next_free_page_no;
    }
    
    buffer_pool_manager_->unpin_page(page_handle.page->get_page_id(),true);

    Rid rid = Rid{.page_no =page_handle.page->get_page_id().page_no,.slot_no = free_slot};

    return rid;

}

/**
 * @description: 在当前表中的指定位置插入一条记录
 * @param {Rid&} rid 要插入记录的位置
 * @param {char*} buf 要插入记录的数据
 */
void RmFileHandle::insert_record(const Rid& rid, char* buf) {


    RmPageHandle page_handle = fetch_page_handle(rid.page_no);

// 补充可能出现的异常代码

    if(rid.page_no > page_handle.file_hdr->num_pages || rid.slot_no > page_handle.file_hdr->num_records_per_page){
        throw RecordNotFoundError(rid.page_no,rid.slot_no);
    }

    // if (!Bitmap::is_set(page_handle.bitmap, rid.slot_no)) {
    //     throw RecordNotFoundError(rid.page_no, rid.slot_no);
    // }
// 以上

	RmRecord record = {page_handle.file_hdr->record_size,page_handle.get_slot(rid.slot_no)};
	record.SetData(buf);

    char* record_data = page_handle.get_slot(rid.slot_no);
    std::memcpy(record_data, buf, file_hdr_.record_size);
    
	Bitmap::set(page_handle.bitmap, rid.slot_no);

    page_handle.page_hdr->num_records++;
    if( page_handle.page_hdr->num_records == file_hdr_.num_records_per_page){//页面已满
    //此处更新下一个空闲页号
        file_hdr_.first_free_page_no = page_handle.page_hdr->next_free_page_no;
    }

    buffer_pool_manager_->unpin_page(page_handle.page->get_page_id(), true);
}



/**
 * @description: 删除记录文件中记录号为rid的记录
 * @param {Rid&} rid 要删除的记录的记录号（位置）
 * @param {Context*} context
 */
void RmFileHandle::delete_record(const Rid& rid, Context* context) {
    // Todo:
    // 1. 获取指定记录所在的page handle
    // 2. 更新page_handle.page_hdr中的数据结构
    // 注意考虑删除一条记录后页面未满的情况，需要调用release_page_handle()

    // 加锁
    if(context!=nullptr){
        context->lock_mgr_->lock_exclusive_on_record(context->txn_,rid,fd_);
    }

    RmPageHandle page_handle = fetch_page_handle(rid.page_no);
    
    // 6/29 补充可能出现的异常抛出，不清楚是否会test报错
    if(rid.page_no > page_handle.file_hdr->num_pages || rid.slot_no > page_handle.file_hdr->num_records_per_page){
        throw RecordNotFoundError(rid.page_no,rid.slot_no);
    }

    // if (!Bitmap::is_set(page_handle.bitmap, rid.slot_no)) {
    //     throw RecordNotFoundError(rid.page_no, rid.slot_no);
    // }
    //以上
    if(page_handle.page_hdr->num_records ==file_hdr_.num_records_per_page){
        // 注意考虑删除一条记录后页面未满的情况，需要调用release_page_handle()
        release_page_handle(page_handle);
    }
    page_handle.page_hdr->num_records--;

    Bitmap::reset(page_handle.bitmap, rid.slot_no);

    // LockDataId lock_da_id = LockDataId{fd_,rid,LockDataType::RECORD};
    // context->txn_->get_lock_set()->insert(lock_da_id);

    buffer_pool_manager_->unpin_page(page_handle.page->get_page_id(),true);


}


/**
 * @description: 更新记录文件中记录号为rid的记录
 * @param {Rid&} rid 要更新的记录的记录号（位置）
 * @param {char*} buf 新记录的数据
 * @param {Context*} context
 */
void RmFileHandle::update_record(const Rid& rid, char* buf, Context* context) {
    // Todo:
    // 1. 获取指定记录所在的page handle
    // 2. 更新记录

    if(context != nullptr){
        context->lock_mgr_->lock_exclusive_on_record(context->txn_,rid,fd_);
    }
    
    RmPageHandle page_handle = fetch_page_handle(rid.page_no);
    // 6/29 补充可能出现的异常抛出，不清楚是否会test报错

    if(rid.page_no > page_handle.file_hdr->num_pages || rid.slot_no > page_handle.file_hdr->num_records_per_page){
        throw RecordNotFoundError(rid.page_no,rid.slot_no);
    }

    // if (!Bitmap::is_set(page_handle.bitmap, rid.slot_no)) {
    //     throw RecordNotFoundError(rid.page_no, rid.slot_no);
    // }
    //以上
    //获取当且页的数据存储指针
    char *slot =  page_handle.get_slot(rid.slot_no);
    memcpy(slot, buf, file_hdr_.record_size);//内存拷贝

    // LockDataId lock_da_id = LockDataId{fd_,rid,LockDataType::RECORD};
    // context->txn_->get_lock_set()->insert(lock_da_id);

    buffer_pool_manager_->unpin_page(page_handle.page->get_page_id(),true);


}

/**
 * 以下函数为辅助函数，仅提供参考，可以选择完成如下函数，也可以删除如下函数，在单元测试中不涉及如下函数接口的直接调用
*/
/**
 * @description: 获取指定页面的页面句柄
 * @param {int} page_no 页面号
 * @return {RmPageHandle} 指定页面的句柄
 */
RmPageHandle RmFileHandle::fetch_page_handle(int page_no) const {
    // Todo:
    // 使用缓冲池获取指定页面，并生成page_handle返回给上层
    // if page_no is invalid, throw PageNotExistError exception


// 补充可能出现的异常
    if (page_no >= file_hdr_.num_pages) throw PageNotExistError(disk_manager_->get_file_name(fd_), page_no);
// 以上

    Page *page = buffer_pool_manager_->fetch_page(PageId{fd_, page_no});//fd来自rm_file_handle

    return RmPageHandle(&file_hdr_, page);
}

/**
 * @description: 创建一个新的page handle
 * @return {RmPageHandle} 新的PageHandle
 */
RmPageHandle RmFileHandle::create_new_page_handle() {
    // Todo:

    // PageId page_id = {.fd = fd_,.page_no = INVALID_PAGE_ID};
    // Page *page = buffer_pool_manager_ ->new_page(&page_id);

    // return RmPageHandle(&file_hdr_, nullptr);
    PageId page_id = {.fd = fd_, .page_no = INVALID_PAGE_ID};
        // 1.使用缓冲池来创建一个新page
    auto new_p = buffer_pool_manager_->new_page(&page_id);


    auto new_page_h = RmPageHandle(&file_hdr_, new_p);

    // 2.更新page handle中的相关信息
    new_page_h.page_hdr->num_records = RM_FILE_HDR_PAGE;
    new_page_h.page_hdr->next_free_page_no = INVALID_PAGE_ID; 
    Bitmap::init(new_page_h.bitmap, new_page_h.file_hdr->bitmap_size);


    // 3.更新file_hdr_
    file_hdr_.num_pages++;
    file_hdr_.first_free_page_no = new_p->get_page_id().page_no;// 更新第一个可用的page_no


    return new_page_h;
}

/**
 * @brief 创建或获取一个空闲的page handle
 *
 * @return RmPageHandle 返回生成的空闲page handle
 * @note pin the page, remember to unpin it outside!
 */
RmPageHandle RmFileHandle::create_page_handle() {
    // Todo:
    // 1. 判断file_hdr_中是否还有空闲页
    //     1.1 没有空闲页：使用缓冲池来创建一个新page；可直接调用create_new_page_handle()
    //     1.2 有空闲页：直接获取第一个空闲页
    // 2. 生成page handle并返回给上层

    // if(file_hdr_.first_free_page_no == -1){//无空闲页情况
    //     return create_new_page_handle();
    // }
    // //返回类型RmPageHandle
    // RmPageHandle page_handle = fetch_page_handle(file_hdr_.first_free_page_no);
    // return RmPageHandle(&file_hdr_, nullptr);
    if (file_hdr_.first_free_page_no == -1) {
        return create_new_page_handle();
    } else {
        //     1.2 有空闲页：直接获取第一个空闲页
        return fetch_page_handle(file_hdr_.first_free_page_no);
    }



}

/**
 * @description: 当一个页面从没有空闲空间的状态变为有空闲空间状态时，更新文件头和页头中空闲页面相关的元数据
 */
void RmFileHandle::release_page_handle(RmPageHandle&page_handle) {
    // Todo:
    // 当page从已满变成未满，考虑如何更新：


    // 1. page_handle.page_hdr->next_free_page_no
    page_handle.page_hdr->next_free_page_no = file_hdr_.first_free_page_no;
    // 2. file_hdr_.first_free_page_no
    file_hdr_.first_free_page_no = page_handle.page->get_page_id().page_no;

}


    
