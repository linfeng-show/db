/* Copyright (c) 2023 Renmin University of China
RMDB is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

#include "rm_scan.h"
#include "rm_file_handle.h"

/**
 * @brief 初始化file_handle和rid
 * @param file_handle
 */
RmScan::RmScan(const RmFileHandle *file_handle) : file_handle_(file_handle) {
    // Todo:
    // 初始化file_handle和rid（指向第一个存放了记录的位置）

    // file_handle_ = file_handle
    rid_ = {.page_no=1,.slot_no = -1};//根据rm_defs.h，页默认从1开始，slot_no未分配前默认为-1
    next();
}

/**
 * @brief 找到文件中下一个存放了记录的位置
 */
void RmScan::next() {
    // Todo:
    // 找到文件中下一个存放了记录的非空闲位置，用rid_来指向这个位置

    while (rid_.page_no < file_handle_->file_hdr_.num_pages) {
        //调用fetch_page_handle，获得page_handle ，我们需要其中的bitmap
        RmPageHandle page_handle = file_handle_->fetch_page_handle(rid_.page_no);
        
        //next_bit bitmap基址 从偏移量slot_no 开始 到偏移量 num_records_per_page 结束，在本页查找吓一跳存放记录的偏移地址
        rid_.slot_no = Bitmap::next_bit(true, page_handle.bitmap, file_handle_->file_hdr_.num_records_per_page, rid_.slot_no);

        if (rid_.slot_no != file_handle_->file_hdr_.num_records_per_page) {//当前page里存在要求位置
            return;//直接return page不需要自增，下次循环还是在本页查找
        }

        //本页不存在非空闲位置
        rid_.slot_no = -1;
        rid_.page_no++;
    }
        rid_.page_no = -1;
}

/**
 * @brief ​ 判断是否到达文件末尾
 */
bool RmScan::is_end() const {
    // Todo: 修改返回值

    return rid_.page_no == -1;//无效页面号
}

/**
 * @brief RmScan内部存放的rid
 */
Rid RmScan::rid() const {
    return rid_;
}