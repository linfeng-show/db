/* Copyright (c) 2023 Renmin University of China
RMDB is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

#include "sm_manager.h"

#include <sys/stat.h>
#include <unistd.h>

#include <fstream>

#include "index/ix.h"
#include "record/rm.h"
#include "record_printer.h"
#include "common/config.h"

/**
 * @description: 判断是否为一个文件夹
 * @return {bool} 返回是否为一个文件夹
 * @param {string&} db_name 数据库文件名称，与文件夹同名
 */
bool SmManager::is_dir(const std::string& db_name) {
    struct stat st;
    return stat(db_name.c_str(), &st) == 0 && S_ISDIR(st.st_mode);
}

/**
 * @description: 创建数据库，所有的数据库相关文件都放在数据库同名文件夹下
 * @param {string&} db_name 数据库名称
 */
void SmManager::create_db(const std::string& db_name) {
    if (is_dir(db_name)) {
        throw DatabaseExistsError(db_name);
    }
    //为数据库创建一个子目录
    std::string cmd = "mkdir " + db_name;
    if (system(cmd.c_str()) < 0) {  // 创建一个名为db_name的目录
        throw UnixError();
    }
    if (chdir(db_name.c_str()) < 0) {  // 进入名为db_name的目录
        throw UnixError();
    }
    //创建系统目录
    DbMeta *new_db = new DbMeta();
    new_db->name_ = db_name;

    // 注意，此处ofstream会在当前目录创建(如果没有此文件先创建)和打开一个名为DB_META_NAME的文件
    std::ofstream ofs(DB_META_NAME);

    // 将new_db中的信息，按照定义好的operator<<操作符，写入到ofs打开的DB_META_NAME文件中
    ofs << *new_db;  // 注意：此处重载了操作符<<

    delete new_db;

    // 创建日志文件
    disk_manager_->create_file(LOG_FILE_NAME);

    // 回到根目录
    if (chdir("..") < 0) {
        throw UnixError();
    }
}

/**
 * @description: 删除数据库，同时需要清空相关文件以及数据库同名文件夹
 * @param {string&} db_name 数据库名称，与文件夹同名
 */
void SmManager::drop_db(const std::string& db_name) {
    if (!is_dir(db_name)) {
        throw DatabaseNotFoundError(db_name);
    }
    std::string cmd = "rm -r " + db_name;
    if (system(cmd.c_str()) < 0) {
        throw UnixError();
    }
}

/**
 * @description: 打开数据库，找到数据库对应的文件夹，并加载数据库元数据和相关文件
 * @param {string&} db_name 数据库名称，与文件夹同名
 */
void SmManager::open_db(const std::string& db_name) {
    if (!is_dir(db_name)) {
        throw DatabaseNotFoundError(db_name);
    }
    // cd to database dir
    if (chdir(db_name.c_str()) < 0) {
        throw UnixError();
    }
    // Load meta
    // 打开一个名为DB_META_NAME的文件
    std::ifstream ifs(DB_META_NAME);
    // 将ofs打开的DB_META_NAME文件中的信息，按照定义好的operator>>操作符，读出到db_中
    ifs >> db_;  // 注意：此处重载了操作符>>
    // Open all record files & index files
    for (auto &entry : db_.tabs_) {
        auto &tab = entry.second;
        // fhs_[tab.name] = rm_manager_->open_file(tab.name);
        fhs_.emplace(tab.name, rm_manager_->open_file(tab.name));
        for (size_t i = 0; i < tab.cols.size(); i++) {
            auto &col = tab.cols[i];
            if (col.index) {
                auto index_name = ix_manager_->get_index_name(tab.name, tab.cols);
                assert(ihs_.count(index_name) == 0);
                // ihs_[index_name] = ix_manager_->open_index(tab.name, i);
                ihs_.emplace(index_name, ix_manager_->open_index(tab.name, tab.cols));
            }
        }
    }

    int fd = disk_manager_->open_file(LOG_FILE_NAME);
    disk_manager_->SetLogFd(fd);
}

/**
 * @description: 把数据库相关的元数据刷入磁盘中
 */
void SmManager::flush_meta() {
    // 默认清空文件
    std::ofstream ofs(DB_META_NAME);
    ofs << db_;
}

/**
 * @description: 关闭数据库并把数据落盘
 */
void SmManager::close_db() {
    // Dump meta
    std::ofstream ofs(DB_META_NAME);
    ofs << db_;  // 注意：此处重载了操作符<<
    db_.name_.clear();
    db_.tabs_.clear();
    // Close all record files
    for (auto &entry : fhs_) {
        rm_manager_->close_file(entry.second.get());
    }
    fhs_.clear();
    // Close all index files
    for (auto &entry : ihs_) {
        ix_manager_->close_index(entry.second.get());
    }
    ihs_.clear();
    disk_manager_->close_file(disk_manager_->GetLogFd());
    if (chdir("..") < 0) {
        throw UnixError();
    }
}

/**
 * @description: 显示所有的表,通过测试需要将其结果写入到output.txt,详情看题目文档
 * @param {Context*} context 
 */
void SmManager::show_tables(Context* context) {

    std::fstream outfile;
    outfile.open("output.txt", std::ios::out | std::ios::app);
    if(IS_OUT){
        outfile << "| Tables |\n";
    }
    
    RecordPrinter printer(1);
    printer.print_separator(context);
    printer.print_record({"Tables"}, context);
    printer.print_separator(context);
    for (auto &entry : db_.tabs_) {
        auto &tab = entry.second;
        printer.print_record({tab.name}, context);
        if(IS_OUT){
            outfile << "| " << tab.name << " |\n";
        }

    }
    printer.print_separator(context);
    outfile.close();
}

/**
 * @description: 显示表的元数据
 * @param {string&} tab_name 表名称
 * @param {Context*} context 
 */
void SmManager::desc_table(const std::string& tab_name, Context* context) {
    TabMeta &tab = db_.get_table(tab_name);

    std::vector<std::string> captions = {"Field", "Type", "Index"};
    RecordPrinter printer(captions.size());
    // Print header
    printer.print_separator(context);
    printer.print_record(captions, context);
    printer.print_separator(context);
    // Print fields
    for (auto &col : tab.cols) {
        std::vector<std::string> field_info = {col.name, coltype2str(col.type), col.index ? "YES" : "NO"};
        printer.print_record(field_info, context);
    }
    // Print footer
    printer.print_separator(context);
}

/**
 * @description: 创建表
 * @param {string&} tab_name 表的名称
 * @param {vector<ColDef>&} col_defs 表的字段
 * @param {Context*} context 
 */
void SmManager::create_table(const std::string& tab_name, const std::vector<ColDef>& col_defs, Context* context) {
    if (db_.is_table(tab_name)) {
        throw TableExistsError(tab_name);
    }
    // Create table meta
    int curr_offset = 0;
    TabMeta tab;
    tab.name = tab_name;
    for (auto &col_def : col_defs) {
        ColMeta col = {.tab_name = tab_name,
                       .name = col_def.name,
                       .type = col_def.type,
                       .len = col_def.len,
                       .offset = curr_offset,
                       .index = false};
        curr_offset += col_def.len;
        tab.cols.push_back(col);
    }
    // Create & open record file
    int record_size = curr_offset;  // record_size就是col meta所占的大小（表的元数据也是以记录的形式进行存储的）
    rm_manager_->create_file(tab_name, record_size);
    db_.tabs_[tab_name] = tab;
    // fhs_[tab_name] = rm_manager_->open_file(tab_name);
    fhs_.emplace(tab_name, rm_manager_->open_file(tab_name));

    flush_meta();

    if(context!=nullptr){
        context->lock_mgr_->lock_exclusive_on_table(context->txn_,fhs_[tab_name]->GetFd());
    }

}
/**
 * @description: 删除表
 * @param {string&} tab_name 表的名称
 * @param {Context*} context
 */
void SmManager::drop_table(const std::string& tab_name, Context* context) {
    auto &tab_meta = db_.get_table(tab_name);
	auto fd = disk_manager_->get_file_fd(tab_name);
	buffer_pool_manager_->delete_all_pages(fd);
	rm_manager_->close_file(fhs_[tab_name].get());
	rm_manager_->destroy_file(tab_name);
	fhs_.erase(tab_name);
	db_.tabs_.erase(tab_name);
	flush_meta();

}

// /**
//  * @description: 创建索引
//  * @param {string&} tab_name 表的名称
//  * @param {vector<string>&} col_names 索引包含的字段名称
//  * @param {Context*} context
//  */
void SmManager::create_index(const std::string& tab_name, const std::vector<std::string>& col_names, Context* context) {
    
    context->lock_mgr_->lock_shared_on_table(context->txn_,fhs_[tab_name]->GetFd());

    TabMeta &tab = db_.get_table(tab_name);
    std::vector<std::string> col_names_copy(col_names.begin(),col_names.end());

    // 先判断索引存不存在，可以遍历出已建立的索引的名字，用vector保存，然后排序，比较之后，一致说名索引已存在
    for(std::vector<IndexMeta>::iterator indexe = tab.indexes.begin();indexe!=tab.indexes.end();indexe++){
        // (*indexe)是对象
        std::vector<std::string> succ_index;
        for(std::vector<ColMeta>::iterator col = (*indexe).cols.begin();col!=(*indexe).cols.end();col++){
            succ_index.push_back((*col).name);
        }
        if(col_names_copy.size()==succ_index.size()){
            // std::sort(col_names_copy.begin(),col_names_copy.end());
            // std::sort(succ_index.begin(),succ_index.end());
            if(std::equal(col_names_copy.begin(),col_names_copy.end(),succ_index.begin(),succ_index.end())){
                throw IndexExistsError(tab_name,col_names);
            }
        }
    }
    // 若建立不存在的索引，则需要添加索引对象
    IndexMeta index;
    index.tab_name = tab_name;
    index.col_num = col_names.size();
    int totalLen = 0;
    for(std::vector<std::string>::const_iterator col = col_names.begin();col!=col_names.end();col++){

        auto single_col = tab.get_col(*col);
        ColMeta col_meta;
        col_meta.tab_name = tab_name;
        col_meta.type = single_col->type;
        col_meta.len = single_col->len;
        col_meta.name = single_col->name;
        col_meta.offset = single_col->offset;
        col_meta.index = true;
        totalLen+=col_meta.len;

        // single_col->index = true;

        index.cols.push_back(col_meta);
    }

    index.col_tot_len = totalLen;
    
    
    tab.indexes.push_back(index);
    
    db_.tabs_[tab_name] = tab;
    // 索引写入!!!!!
    flush_meta();

    ix_manager_->create_index(tab_name,index.cols);

    // tab.del_v_index.push_back(col_names);

    // // 打开index文件
    auto ih = ix_manager_->open_index(tab_name,col_names);

    // // 获取记录文件句柄
    auto file_handle = fhs_.at(tab_name).get();
    int offset = 0;
    char *key = new char[totalLen];
    for(RmScan rm_scan(file_handle); !rm_scan.is_end(); rm_scan.next()) {
        offset = 0;
        auto rec = file_handle->get_record(rm_scan.rid(), context);  
        for(int i = 0;i<col_names.size();i++){
            memcpy(key+offset,rec->data+index.cols[i].offset,index.cols[i].len);
            offset+=index.cols[i].len;
        }
        ih->insert_entry(key, rm_scan.rid(), context->txn_);
    }

    // // Store index handle
    auto index_name = ix_manager_->get_index_name(tab_name, index.cols);
    
    assert(ihs_.count(index_name) == 0);
    ihs_.emplace(index_name, std::move(ih));

    // delete[] key;
}

// /**
//  * @description: 删除索引
//  * @param {string&} tab_name 表名称
//  * @param {vector<string>&} col_names 索引包含的字段名称
//  * @param {Context*} context
//  */
void SmManager::drop_index(const std::string& tab_name, const std::vector<std::string>& col_names, Context* context) {

    if(!db_.is_table(tab_name)) {
        throw TableNotFoundError(tab_name);
    }
    context->lock_mgr_->lock_shared_on_table(context->txn_,fhs_[tab_name]->GetFd());

    TabMeta &tab = db_.get_table(tab_name);
    
    if(tab.indexes.size()==0){
        throw IndexNotFoundError(tab_name, col_names);
        return;
    }
    
    for(auto col_name : col_names) {
        auto col = tab.get_col(col_name);
        bool found = false;
        for(int i = 0;i<tab.indexes.size();i++){
            auto &col = tab.indexes[i].cols;
            auto it = tab.indexes[i].cols.begin();
            while(it!=tab.indexes[i].cols.begin()){
                if(it->name==col_name){
                    found = true;
                    break;
                }
                it++;
            }
            if(found){
                break;
            }
        }
        if(!found){
            col->index = false;
        }
    }

    auto index_name = ix_manager_->get_index_name(tab_name, col_names);
    if(ihs_.count(index_name) > 0) {
        ix_manager_->close_index(ihs_.at(index_name).get());

        ix_manager_->destroy_index(tab_name, col_names);

        IxIndexHandle *ih = ihs_.at(index_name).get();
        buffer_pool_manager_->delete_pages_index(ih->getFd_());

        auto ix_meta = db_.get_table(tab_name).get_index_meta(col_names);
        db_.get_table(tab_name).indexes.erase(ix_meta);
        ihs_.erase(index_name);
    }

}

// /**
//  * @description: 删除索引
//  * @param {string&} tab_name 表名称
//  * @param {vector<ColMeta>&} 索引包含的字段元数据
//  * @param {Context*} context
//  */
// 删除表的时候删除index
void SmManager::drop_index(const std::string& tab_name, const std::vector<ColMeta>& cols, Context* context) {
    
    // 此处为删除表的时候的删除，所以不需要判断是否存在，直接删除就好了
    std::vector<std::string> col_names;
    for (std::vector<ColMeta>::const_iterator it = cols.begin(); it != cols.end(); ++it) {
        col_names.push_back((*it).name);
    }
    drop_index(tab_name,col_names,context);

}
// 07/11 增加showindex逻辑功能
/**
 * 显示指定表的索引信息 succ_index.push_back((*col).name);
 * @param tab_name 表名
 * @param context 上下文
*/
void SmManager::show_index(const std::string& tab_name, Context* context){

    
    // index应该从tab.indexes中获取

    // 获取表meta信息
    TabMeta &tab = db_.get_table(tab_name);


    std::fstream outfile;
    outfile.open("output.txt", std::ios::out | std::ios::app);
    RecordPrinter printer(3);

    for(std::vector<IndexMeta>::iterator indexe = tab.indexes.begin();indexe!=tab.indexes.end();indexe++){
        // (*indexe)是对象
        std::string str_index = "";
        
        for(std::vector<ColMeta>::iterator col = (*indexe).cols.begin();col!=(*indexe).cols.end();col++){
            if(str_index.length()!=0){
                str_index+=",";
            }
            str_index+=(*col).name;
        }

        printer.print_record({tab_name,"unique","("+str_index+")"}, context);
        if(IS_OUT){
            outfile << "| " << tab_name << " | " << "unique" << " | " << "("+str_index+")" << " |\n";
        }

    
    }

    printer.print_separator(context);

    outfile.close();

}


