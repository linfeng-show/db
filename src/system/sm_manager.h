/* Copyright (c) 2023 Renmin University of China
RMDB is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

#pragma once

#include "index/ix.h"
#include "record/rm_file_handle.h"
#include "sm_defs.h"
#include "sm_meta.h"
#include "common/context.h"

#include "record/rm.h"
#include "record_printer.h"

class Context;

struct ColDef {
    std::string name;  // Column name
    ColType type;      // Type of column
    int len;           // Length of column
};

/* 系统管理器，负责元数据管理和DDL语句的执行 */
class SmManager {
   public:
    DbMeta db_;             // 当前打开的数据库的元数据
    std::unordered_map<std::string, std::unique_ptr<RmFileHandle>> fhs_;    // file name -> record file handle, 当前数据库中每张表的数据文件
    std::unordered_map<std::string, std::unique_ptr<IxIndexHandle>> ihs_;   // file name -> index file handle, 当前数据库中每个索引的文件
    bool whether_write_record = true;
   private:
    DiskManager* disk_manager_;
    BufferPoolManager* buffer_pool_manager_;
    RmManager* rm_manager_;
    IxManager* ix_manager_;

   public:
    SmManager(DiskManager* disk_manager, BufferPoolManager* buffer_pool_manager, RmManager* rm_manager,
              IxManager* ix_manager)
        : disk_manager_(disk_manager),
          buffer_pool_manager_(buffer_pool_manager),
          rm_manager_(rm_manager),
          ix_manager_(ix_manager) {}

    ~SmManager() {}

    
    void recover_create_index(const std::string& tab_name, const std::vector<std::string>& col_names){
        
        
        TabMeta &tab = db_.get_table(tab_name);
        
        IndexMeta index;
        index.tab_name = tab_name;
        index.col_num = col_names.size();
        int totalLen = 0;
        for(std::vector<std::string>::const_iterator col = col_names.begin();col!=col_names.end();col++){

            auto single_col = tab.get_col(*col);
            ColMeta col_meta;
            col_meta.tab_name = tab_name;
            col_meta.type = single_col->type;
            col_meta.len = single_col->len;
            col_meta.name = single_col->name;
            col_meta.offset = single_col->offset;
            col_meta.index = true;
            totalLen+=col_meta.len;

            // single_col->index = true;

            index.cols.push_back(col_meta);
        }

        index.col_tot_len = totalLen;

        // 这一句控制了有没有索引?
        tab.indexes.push_back(index);

        ix_manager_->create_index(tab_name,index.cols);
        
        auto ih = ix_manager_->open_index(tab_name,col_names);

        // std::cout<<"获取记录文件句柄"<<std::endl;
        // 获取记录文件句柄
        auto file_handle = fhs_.at(tab_name).get();

        int offset = 0;
        
        char *key = new char[totalLen];

        for(RmScan rm_scan(file_handle); !rm_scan.is_end(); rm_scan.next()) {
            offset = 0;
            auto rec = file_handle->get_record(rm_scan.rid(), nullptr);  
            for(int i = 0;i<col_names.size();i++){
                memcpy(key+offset,rec->data+index.cols[i].offset,index.cols[i].len);
                offset+=index.cols[i].len;
            }
            try{
               // std::cout<<"插入一个插入一个插入一个插入一个插入一个插入一个"<<std::endl;
                ih->insert_entry(key, rm_scan.rid(), nullptr);
            }catch(InternalError &err){
                auto fh_ = fhs_.at(tab_name).get();
                fh_->delete_record(rm_scan.rid(),nullptr);
            }
            
        }

        // // Store index handle
        auto index_name = ix_manager_->get_index_name(tab_name, index.cols);
        
        assert(ihs_.count(index_name) == 0);
        ihs_.emplace(index_name, std::move(ih));

    }



    BufferPoolManager* get_bpm() { return buffer_pool_manager_; }

    RmManager* get_rm_manager() { return rm_manager_; }  

    IxManager* get_ix_manager() { return ix_manager_; }  

    bool is_dir(const std::string& db_name);

    void create_db(const std::string& db_name);

    void drop_db(const std::string& db_name);

    void open_db(const std::string& db_name);

    void close_db();

    void flush_meta();

    void show_tables(Context* context);

    void desc_table(const std::string& tab_name, Context* context);

    void create_table(const std::string& tab_name, const std::vector<ColDef>& col_defs, Context* context);

    void drop_table(const std::string& tab_name, Context* context);

    void create_index(const std::string& tab_name, const std::vector<std::string>& col_names, Context* context);

    void drop_index(const std::string& tab_name, const std::vector<std::string>& col_names, Context* context);
    
    void drop_index(const std::string& tab_name, const std::vector<ColMeta>& col_names, Context* context);

    void show_index(const std::string& tab_name, Context* context);

    // void load_data(const std::string& tab_name, const std::string& file_name, Context* context);
};
