/* Copyright (c) 2023 Renmin University of China
RMDB is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

#include "log_recovery.h"

/**
 * @description: analyze阶段，需要获得脏页表（DPT）和未完成的事务列表（ATT）
 */
void RecoveryManager::analyze() {
    
    char buf[LOG_BUFFER_SIZE];
    int fi_off = 0;
    int buf_off = 0;
    int read_size = 0;
    while(true){
      std::fill(buf, buf + LOG_BUFFER_SIZE, 0);
      buf_off = 0;
      read_size = disk_manager_->read_log(buf,LOG_BUFFER_SIZE,fi_off);

      if(read_size>=0){
         while(buf_off+OFFSET_LOG_TOT_LEN<read_size){
            if(*reinterpret_cast<uint32_t*>(buf+buf_off+OFFSET_LOG_TOT_LEN)+buf_off>read_size){
              break;
            }else{
              LogType log_type_;
              std::memcpy(&log_type_, buf + buf_off + OFFSET_LOG_TYPE, sizeof(LogType));       

              if (log_type_ == LogType::INSERT || log_type_ == LogType::DELETE || log_type_ == LogType::UPDATE) {
                LogRecord* log_rcd;
                if (log_type_ == LogType::INSERT) {
                    //std::cout<<"读到insert的日志"<<std::endl;
                    log_rcd = new InsertLogRecord();
                } else if (log_type_ == LogType::DELETE) {
                    //std::cout<<"读到delete的日志"<<std::endl;
                    log_rcd = new DeleteLogRecord();
                } else {
                    //std::cout<<"读到update的日志"<<std::endl;
                    log_rcd = new UpdateLogRecord();
                }

                log_rcd->deserialize(buf + buf_off);
                lsn_off_map[log_rcd->lsn_] = fi_off;
                lsn_size_map[log_rcd->lsn_] = log_rcd->log_tot_len_;
                fi_off += log_rcd->log_tot_len_;
                buf_off += log_rcd->log_tot_len_;
                undo_lsn_map[log_rcd->log_tid_] = log_rcd->lsn_;
                redo_ve.emplace_back(log_rcd->lsn_);

              } else if (log_type_ == LogType::begin || log_type_ == LogType::commit || log_type_ == LogType::ABORT) {
                LogRecord* log_rcd;
                if (log_type_ == LogType::begin) {
                    //std::cout<<"begin"<<std::endl;
                    log_rcd = new BeginLogRecord();
                } else if (log_type_ == LogType::commit) {
                    //std::cout<<"commit"<<std::endl;
                    log_rcd = new CommitLogRecord();
                } else {
                    //std::cout<<"abort"<<std::endl;
                    log_rcd = new AbortLogRecord();
                }

                log_rcd->deserialize(buf + buf_off);
                lsn_off_map[log_rcd->lsn_] = fi_off;
                lsn_size_map[log_rcd->lsn_] = log_rcd->log_tot_len_;
                fi_off += log_rcd->log_tot_len_;
                buf_off += log_rcd->log_tot_len_;

                if (log_type_ == LogType::begin||log_type_ == LogType::ABORT) {
                   // std::cout<<"undo添加的lsn: "<<log_rcd->log_tid_<<std::endl;
                    undo_lsn_map[log_rcd->log_tid_] = log_rcd->lsn_;
                } else if(log_type_ == LogType::commit) {
                    //std::cout<<"undo删去的lsn: "<<log_rcd->log_tid_<<std::endl;
                    undo_lsn_map.erase(log_rcd->log_tid_);
                }
              }
            }
          }
      }else{
        break;
      }
      if(read_size<LOG_BUFFER_SIZE){
        break;
      }
    }

      std::string prefix = "";
      std::string suffix = ".idx";

      DIR* directory = opendir(".");

      if (directory) {
          dirent* file;
          while ((file = readdir(directory)) != nullptr) {
              std::string filename = file->d_name;
              if (filename.length() >= suffix.length() && filename.substr(0, prefix.length()) == prefix && filename.substr(filename.length() - suffix.length()) == suffix) {
                  
                  if(remove(filename.c_str())!=0){
                    //std::cout<<"文件删除失败";
                  }else{
                    //std::cout<<"文件删除成功";
                  }
              }
          }
          closedir(directory);
      }
}
/**
 * @description: 重做所有未落盘的操作
 */
void RecoveryManager::redo() {
    for(lsn_t lsn:redo_ve){
      redo_i_d_u(lsn);
    }
}


/**
 * @description: 回滚未完成的事务
 */
void RecoveryManager::undo() {

      //std::cout<<undo_lsn_map.size()<<std::endl;
      for(auto un : undo_lsn_map){
        undo_i_d_u(un.second);
      }

      for (auto it = sm_manager_->db_.tabs_.begin(); it != sm_manager_->db_.tabs_.end(); ++it) {
          
          // 表
          TabMeta &tab = sm_manager_->db_.get_table(it->first);
          
          std::vector<std::string> col_names;
          col_names.clear();
          for(auto index : tab.indexes){

              for(auto indexCol:index.cols){
                col_names.push_back(indexCol.name);
              }
              sm_manager_->recover_create_index(tab.name,col_names);
              col_names.clear();
          }
      }
}
