/* Copyright (c) 2023 Renmin University of China
RMDB is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

#pragma once

#include <map>
#include <unordered_map>
#include "log_manager.h"
#include "storage/disk_manager.h"
#include "system/sm_manager.h"
#include "transaction/transaction_manager.h"
#include <dirent.h>
#include <cstring>

class RedoLogsInPage {
public:
    RedoLogsInPage() { table_file_ = nullptr; }
    RmFileHandle* table_file_;
    std::vector<lsn_t> redo_logs_;   // 在该page上需要redo的操作的lsn
};


class RecoveryManager {
public:

    RecoveryManager(DiskManager* disk_manager, BufferPoolManager* buffer_pool_manager, SmManager* sm_manager, TransactionManager* txn_manager) {
        disk_manager_ = disk_manager;
        buffer_pool_manager_ = buffer_pool_manager;
        sm_manager_ = sm_manager;
        txn_manager_ = txn_manager;
    }
    void analyze();
    void redo();
    void undo();

    void undo_i_d_u(lsn_t lsn){
        while(lsn!=INVALID_LSN){
          char* buf = new char[lsn_size_map[lsn]];
          disk_manager_->read_log(buf,lsn_size_map[lsn],lsn_off_map[lsn]);
          LogType log_type = *reinterpret_cast<LogType*>(buf);
          if(log_type==LogType::INSERT){
            
            //std::cout<<"回滚了一个插入"<<std::endl;
            InsertLogRecord insert = InsertLogRecord();
            insert.deserialize(buf);

            RmFileHandle *fh_ =sm_manager_->fhs_[insert.table_name_].get();
            fh_->delete_record(insert.rid_,nullptr);
            lsn = insert.prev_lsn_;

          }else if(log_type==LogType::DELETE){

            //std::cout<<"回滚了一个删除"<<std::endl;
            
            DeleteLogRecord de = DeleteLogRecord();
            de.deserialize(buf);

            RmFileHandle *fh_ =sm_manager_->fhs_[de.table_name_].get();

            fh_->insert_record(de.rid_,de.delete_value_.data);
            lsn = de.prev_lsn_;


          }else if(log_type==LogType::UPDATE){
            
           // std::cout<<"回滚了一个修改"<<std::endl;

            UpdateLogRecord up = UpdateLogRecord();
            up.deserialize(buf);

            RmFileHandle *fh_ =sm_manager_->fhs_[up.table_name_].get();

            fh_->update_record(up.rid_,up.old_value_.data,nullptr);

            lsn = up.prev_lsn_;

          }else if(log_type==LogType::begin){
            
            break;

          }else if(log_type == LogType::ABORT){
            AbortLogRecord abort = AbortLogRecord();
            abort.deserialize(buf);
            lsn = abort.prev_lsn_;
          }

        }
    }

    void redo_i_d_u(lsn_t lsn){
        char* buf = new char[lsn_size_map[lsn]];
        disk_manager_->read_log(buf,lsn_size_map[lsn],lsn_off_map[lsn]);
        LogType log_type = *reinterpret_cast<LogType*>(buf);
        if(log_type==LogType::INSERT){
        
        InsertLogRecord insert = InsertLogRecord();
        insert.deserialize(buf);

        RmFileHandle *fh_ =sm_manager_->fhs_[std::string(insert.table_name_,insert.table_name_size_)].get();
        fh_->insert_record(insert.insert_value_.data,nullptr);

      }else if(log_type==LogType::DELETE){
        
        DeleteLogRecord de = DeleteLogRecord();
        de.deserialize(buf);

        RmFileHandle *fh_ =sm_manager_->fhs_[std::string(de.table_name_,de.table_name_size_)].get();

        RmPageHandle rph = fh_->fetch_page_handle(de.rid_.page_no);
        if(rph.page->get_page_lsn()<de.lsn_){
          fh_->delete_record(de.rid_,nullptr);
        }
        buffer_pool_manager_->unpin_page(rph.page->get_page_id(),true);


      }else if(log_type==LogType::UPDATE){

        UpdateLogRecord up = UpdateLogRecord();
        up.deserialize(buf);

        RmFileHandle *fh_ =sm_manager_->fhs_[std::string(up.table_name_,up.table_name_size_)].get();

        RmPageHandle rph = fh_->fetch_page_handle(up.rid_.page_no);
        if(rph.page->get_page_lsn()<up.lsn_){
          fh_->delete_record(up.rid_,nullptr);
        }
        buffer_pool_manager_->unpin_page(rph.page->get_page_id(),true);
      }
    }

  
private:
    LogBuffer buffer_;                                              // 读入日志
    DiskManager* disk_manager_;                                     // 用来读写文件
    BufferPoolManager* buffer_pool_manager_;                        // 对页面进行读写
    SmManager* sm_manager_;                                         // 访问数据库元数据
    TransactionManager* txn_manager_; 
    std::map<txn_id_t, lsn_t> undo_lsn_map;
    std::map<lsn_t, int> lsn_off_map;
    std::map<lsn_t, int> lsn_size_map;
    std::vector<lsn_t> redo_ve;
    
};
