/* Copyright (c) 2023 Renmin University of China
RMDB is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

#pragma once

#include <cassert>
#include <cstring>
#include <memory>
#include <string>
#include <vector>
#include "defs.h"
#include "record/rm_defs.h"

#include <cstdint>
#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>




struct TabCol {
    std::string tab_name;
    std::string col_name;
    int JHName=0;
    std::string newcolname="0";

    friend bool operator<(const TabCol &x, const TabCol &y) {
        return std::make_pair(x.tab_name, x.col_name) < std::make_pair(y.tab_name, y.col_name);
    }
};

struct DateTime{

// �ڲ���Ա����
  int year_; 
  int month_;
  int day_;
  int hour_;
  int minute_;
  int second_;


uint64_t Encode() const {
  uint64_t value = 0;
  value |= static_cast<uint64_t>(year_) << 40;
  value |= static_cast<uint64_t>(month_) << 32;
  value |= static_cast<uint64_t>(day_) << 24;
  value |= static_cast<uint64_t>(hour_) << 16;
  value |= static_cast<uint64_t>(minute_) << 8;
  value |= static_cast<uint64_t>(second_);
  return value;
}

 

// ����
void Decode(uint64_t code) {
  year_ = static_cast<int>((code >> 40) & 0xFFFF);
  month_ = static_cast<int>((code >> 32) & 0xFF);
  day_ = static_cast<int>((code >> 24) & 0xFF);
  hour_ = static_cast<int>((code >> 16) & 0xFF);
  minute_ = static_cast<int>((code >> 8) & 0xFF);
  second_ = static_cast<int>(code & 0xFF);
}

// ��ʽ�����ʱ��
std::string ToString() const {
    std::stringstream ss;
 
    ss << year_ << "-";
    ss << std::setfill('0') << std::setw(2) << month_ << "-";
    ss << std::setfill('0') << std::setw(2) << day_ << " "; 
    ss << std::setfill('0') << std::setw(2) << hour_<< ":";
    ss << std::setfill('0') << std::setw(2) << minute_<< ":";
    ss << std::setfill('0') << std::setw(2) << second_;
    return ss.str();
  }

// ���ַ����л�ȡdetetime��Ϣ
void FromString(const std::string& datetime) {
  std::istringstream iss(datetime);
 
  char delimiter;
  iss >> year_ >> delimiter >> month_ >> delimiter >> day_
     >> hour_ >> delimiter >> minute_ >> delimiter >> second_;

}



// �Ϸ����ж�
bool IsValid(const DateTime& dt) const {
  if (dt.year_ < 1000 || dt.year_ > 9999) {
    return false;
  }

  if (dt.month_ < 1 || dt.month_ > 12) {
    return false;
  }

  if(dt.month_ == 2 && dt.day_ > 29){
    return false;
  }

  if (dt.day_ < 1 || dt.day_ > 31) {
    return false;  
  }

  if (dt.hour_ > 23) {
    return false;
  }

  if (dt.minute_ > 59) {
    return false;
  }
  
  if (dt.second_ > 59) {
    return false;
  }

  // �Ϸ�
  return true; 
}

// �����������
    bool operator>(const DateTime& rhs) const {
      //std::cout<<Encode()<<">"<<rhs.Encode()<<std::endl;
        return Encode() > rhs.Encode();
    }

    bool operator<(const DateTime& rhs) const {

        return Encode() < rhs.Encode();
    }


    bool operator==(const DateTime& rhs) const {

        return Encode() == rhs.Encode();
    }

    bool operator>=(const DateTime& rhs) const {

        return Encode() >= rhs.Encode();
    }


    bool operator<=(const DateTime& rhs) const {

        return Encode() <= rhs.Encode();
    }

};

struct Value {
    ColType type;  // type of value
    union {
        int int_val;      // int value
        float float_val;  // float value
        int64_t bigint_val;
    };
    std::string str_val;  // string value

    std::shared_ptr<RmRecord> raw;  // raw record buffer

    DateTime datetime;  // datetime ʱ����

    void set_int(int int_val_) {
        type = TYPE_INT;
        int_val = int_val_;
    }

    void set_float(float float_val_) {
        type = TYPE_FLOAT;
        float_val = float_val_;
    }

    void set_str(std::string str_val_) {
        type = TYPE_STRING;
        str_val = std::move(str_val_);
    }
    
    // bigint 
    void set_bigint(int64_t bigint_val_) {
        type = TYPE_BIGINT;
        try{
            bigint_val = std::move(bigint_val_);
        }catch(std::out_of_range const& ex) {
            throw BigintOverflowError();
        }
        
    }
    void set_bigint(const std::string bigint_val_) {
        type = TYPE_BIGINT;
         try {
            bigint_val = std::stoll(bigint_val_);
        }catch(std::out_of_range const& ex) {
            throw BigintOverflowError();
        }

    }

    // datetime
    void set_datetime(const std::string datetime_str){
        type = TYPE_DATETIME;
        // ��������ַ�����װ��datetime��
        datetime.FromString(datetime_str);
        if(!datetime.IsValid(datetime)){ // �Ϸ����ж�
            throw DateIllegalError(datetime.ToString());
        }
    }





    void init_raw(int len) {
        assert(raw == nullptr);
        raw = std::make_shared<RmRecord>(len);
        if (type == TYPE_INT) {
            assert(len == sizeof(int));
            *(int *)(raw->data) = int_val;
        } else if (type == TYPE_FLOAT) {
            assert(len == sizeof(float));
            *(float *)(raw->data) = float_val;
        } else if (type == TYPE_STRING) {
            if (len < (int)str_val.size()) {
                throw StringOverflowError();
            }
            memset(raw->data, 0, len);
            memcpy(raw->data, str_val.c_str(), str_val.size());
        }else if (type == TYPE_BIGINT){
            // ����Ӧ�ÿ��Խ��г����жϣ����ַ������Ͳ��
            assert(len == sizeof(int64_t));
            *(int64_t *)(raw->data) = bigint_val;
        }else if(type == TYPE_DATETIME){
            assert(len == sizeof(uint64_t));
            *(uint64_t *)(raw->data) = datetime.Encode();
        }
    }

    // 重载运算符
    bool operator>(const Value &rhs_val){

       // std::cout<<"大于号"<<std::endl;

        if(rhs_val.type == TYPE_INT){

            return int_val > rhs_val.int_val;

        }else if(rhs_val.type == TYPE_FLOAT){

            return float_val > rhs_val.float_val;

        }else if(rhs_val.type == TYPE_STRING){

            return strcmp(str_val.c_str(), rhs_val.str_val.c_str()) > 0;

        }else{

            throw UnixError();
            
        }
    }


    bool operator<(const Value &rhs_val){

        //std::cout<<"小于号"<<std::endl;

        if(rhs_val.type == TYPE_INT){

            return int_val < rhs_val.int_val;

        }else if(rhs_val.type == TYPE_FLOAT){

            return float_val < rhs_val.float_val;

        }else if(rhs_val.type == TYPE_STRING){

            return strcmp(str_val.c_str(), rhs_val.str_val.c_str()) < 0;

        }else{

            throw UnixError();
            
        }
    }


    bool operator>=(const Value &rhs_val){

        //std::cout<<"大于等于号"<<std::endl;

        if(rhs_val.type == TYPE_INT){

            return int_val >= rhs_val.int_val;

        }else if(rhs_val.type == TYPE_FLOAT){

            return float_val >= rhs_val.float_val;

        }else if(rhs_val.type == TYPE_STRING){

            return strcmp(str_val.c_str(), rhs_val.str_val.c_str()) >= 0;

        }else{

            throw UnixError();
            
        }
    }

    bool operator<=(const Value &rhs_val){


        //std::cout<<"小于等于号"<<std::endl;

        if(rhs_val.type == TYPE_INT){

            return int_val <= rhs_val.int_val;

        }else if(rhs_val.type == TYPE_FLOAT){

            return float_val <= rhs_val.float_val;

        }else if(rhs_val.type == TYPE_STRING){

            return strcmp(str_val.c_str(), rhs_val.str_val.c_str()) <= 0;

        }else{

            throw UnixError();
            
        }
    }


    bool operator==(const Value &rhs_val){

        //std::cout<<"等于号"<<std::endl;

        if(rhs_val.type == TYPE_INT){

            return int_val == rhs_val.int_val;

        }else if(rhs_val.type == TYPE_FLOAT){

            return float_val == rhs_val.float_val;

        }else if(rhs_val.type == TYPE_STRING){

            return strcmp(str_val.c_str(), rhs_val.str_val.c_str()) == 0;

        }else{

            throw UnixError();
            
        }
    }

    bool operator!=(const Value &rhs_val){

       // std::cout<<"不等号"<<std::endl;
        return !(*this==rhs_val);
    }

};


enum CompOp { OP_EQ, OP_NE, OP_LT, OP_GT, OP_LE, OP_GE };

struct Condition {
    TabCol lhs_col;   // left-hand side column
    CompOp op;        // comparison operator
    bool is_rhs_val;  // true if right-hand side is a value (not a column)
    TabCol rhs_col;   // right-hand side column
    Value rhs_val;    // right-hand side value
};

struct SetClause {
    TabCol lhs;
    Value rhs;
    int cpy=0;
    int si=0;

};