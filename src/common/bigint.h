#include <bits/stdc++.h>
class bigint {
    //BIGINT

public:
    
    long long digits;

    void isOverFlow(long long num){
        if((num > 9223372036854775807)||(num < -9223372036854775808)){
            throw std::out_of_range("failure");
        }
    }

    bigint(long long num) {
        digits = num;
        this->isOverFlow(digits);
    }

    bigint(std::string strNum) {

        digits = 0;
        if(strNum == "0"){
            return;
        }
            
        bool isNegative = strNum[0] == '-';

        std::reverse(strNum.begin(), strNum.end());
        int len = (isNegative ? strNum.length()-1 : strNum.length());
        for (int i = 0; i < len; ++i) {
            digits+=(strNum[i] - '0')*pow(10,i);
        }
        if(isNegative){
            digits = -digits;
        }
        
        this->isOverFlow(digits);

    }

    bigint operator+(const bigint& other) const {

        bigint result = 0;
        result.digits = digits + other.digits;

        if((digits > 9223372036854775807)||(digits < -9223372036854775808)){
            throw std::out_of_range("failure");
        }
        return result;
        
    }

    bigint operator-(const bigint& other) const{
        bigint result = 0;
        result.digits = digits - other.digits;

        if((digits > 9223372036854775807)||(digits < -9223372036854775808)){
            throw std::out_of_range("failure");
        }
        return result;
    }

    bigint operator*(const bigint& other) const{
        bigint result = 0;
        result.digits = digits * other.digits;

        if((digits > 9223372036854775807)||(digits < -9223372036854775808)){
            throw std::out_of_range("failure");
        }

        return result;
    }

    bigint operator/(const bigint& other) const{
        bigint result = 0;
        result.digits = digits / other.digits;

        if((digits > 9223372036854775807)||(digits < -9223372036854775808)){
            throw std::out_of_range("failure");
        }

        return result;
    }
    
    bool operator==(const bigint& other) const{
        return digits == other.digits;
    }

    bool operator!=(const bigint& other) const{
        return digits != other.digits;
    }


    bool operator<(const bigint& other) const{
        return digits < other.digits;
    }
    
    bool operator<=(const bigint& other) const{
        return digits <= other.digits;
    }

    bool operator>(const bigint& other) const{
        return digits > other.digits;
    }
    
    bool operator>=(const bigint& other) const{
        return digits >= other.digits;
    }


    friend std::ostream& operator<<(std::ostream& os, const bigint& num);

};


std::ostream& operator<<(std::ostream& os, const bigint& num) {

    os<<num.digits;
    return os;

}