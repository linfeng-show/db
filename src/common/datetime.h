// #ifndef DATETIME
// #define DATETIME

// #include <cstdint>
// #include <string>
// #include <sstream>
// #include <iomanip>
// #include <iostream>

// // 日期类
// class DateTime {
// public:
//   // 构造函数
//   inline DateTime(int year , int month , int day , 
//            int hour, int minute , int second );
//   // 将日期时间编码为uint64_t
//   inline uint64_t Encode() const;
// // 从编码中解码出日期时间
//  inline void Decode(uint64_t code);
// // 格式化输出日期时间字符串
//  inline std::string ToString() const;
//  // 从字符串解析输入日期时间 
//  inline void FromString(const std::string& datetime);
// // 合法性判断
//  inline bool IsValid(const DateTime& dt) const;

// // 重载比较运算符
//  inline bool operator>(const DateTime& rhs) const;

//  inline bool operator<(const DateTime& rhs) const;

//  inline bool operator==(const DateTime& rhs) const;

//  inline bool operator>=(const DateTime& rhs) const;

//  inline bool operator<=(const DateTime& rhs) const;

// private:
// // 内部成员变量

//  int year_; 
//   int month_;
//   int day_;
//   int hour_;
//  int minute_;
//   int second_;
// };

// // 构造函数
// inline DateTime::DateTime(int year = 1000, int month = 1, int day = 1, 
//                    int hour = 0, int minute = 0, int second = 0) {
//   year_ = static_cast<int>(year);
//   month_ = static_cast<int>(month);
//   day_ = static_cast<int>(day);
//   hour_ = static_cast<int>(hour);
//   minute_ = static_cast<int>(minute);
//   second_ = static_cast<int>(second);
// }

// inline uint64_t DateTime::Encode() const {
//   uint64_t value = 0;
//   value |= static_cast<uint64_t>(year_) << 40;
//   value |= static_cast<uint64_t>(month_) << 32;
//   value |= static_cast<uint64_t>(day_) << 24;
//   value |= static_cast<uint64_t>(hour_) << 16;
//   value |= static_cast<uint64_t>(minute_) << 8;
//   value |= static_cast<uint64_t>(second_);
//   return value;
// }

// // 解码
// inline void DateTime::Decode(uint64_t code) {
//   year_ = static_cast<uint16_t>(code >> 40);
//   month_ = static_cast<uint8_t>(code >> 32);
//   day_ = static_cast<uint8_t>(code >> 24);
//   hour_ = static_cast<uint8_t>(code >> 16);
//   minute_ = static_cast<uint8_t>(code >> 8);
//   second_ = static_cast<uint8_t>(code);
// }

// // 格式化输出时间
// inline std::string DateTime::ToString() const {
//     std::stringstream ss;
//     ss << year_ << "-";
//     ss << std::setfill('0') << std::setw(2) << month_ << "-";
//     ss << std::setfill('0') << std::setw(2) << day_ << " "; 
//     ss << std::setfill('0') << std::setw(2) << hour_<< ":";
//     ss << std::setfill('0') << std::setw(2) << minute_<< ":";
//     ss << std::setfill('0') << std::setw(2) << second_;
//     return ss.str();
//   }

// // 从字符串中获取detetime信息
// inline void DateTime::FromString(const std::string& datetime) {
//   std::istringstream iss(datetime);
 
//   char delimiter;
//   iss >> year_ >> delimiter >> month_ >> delimiter >> day_
//      >> hour_ >> delimiter >> minute_ >> delimiter >> second_;
// }

// // 合法性判断
// inline bool DateTime::IsValid(const DateTime& dt) const {
//   if (dt.year_ < 1000 || dt.year_ > 9999) {
//     return false;
//   }

//   if (dt.month_ < 1 || dt.month_ > 12) {
//     return false;
//   }

//   if (dt.day_ < 1 || dt.day_ > 31) {
//     return false;  
//   }

//   if (dt.hour_ > 23) {
//     return false;
//   }

//   if (dt.minute_ > 59) {
//     return false;
//   }
  
//   if (dt.second_ > 59) {
//     return false;
//   }

//   // 合法
//   return true; 
// }

// // 重载了 > < >= <= 等运算符，
//     inline bool DateTime::operator>(const DateTime& rhs) const {
//       std::cout<<Encode()<<">"<<rhs.Encode()<<std::endl;
//         return Encode() > rhs.Encode();
//     }

//     inline bool DateTime::operator<(const DateTime& rhs) const {
//       std::cout<<Encode()<<"<"<<rhs.Encode()<<std::endl;
//         return Encode() < rhs.Encode();
//     }


//     inline bool DateTime::operator==(const DateTime& rhs) const {
//         std::cout<<Encode()<<"=="<<rhs.Encode()<<std::endl;
//         return Encode() == rhs.Encode();
//     }

//     inline bool DateTime::operator>=(const DateTime& rhs) const {
//       std::cout<<Encode()<<">="<<rhs.Encode()<<std::endl;
//         return Encode() >= rhs.Encode();
//     }


//     inline bool DateTime::operator<=(const DateTime& rhs) const {
//       std::cout<<Encode()<<"<="<<rhs.Encode()<<std::endl;
//         return Encode() <= rhs.Encode();
//     }


// #endif // DATETIME