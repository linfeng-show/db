#include "record/rm_defs.h"

// 07/19 定义joinbuffer 解决第八题块嵌套循环超时问题
class JoinBuffer {  // 连接块缓冲池
public:
    static const int join_buffer_size = 1000; // 缓冲池大小
    std::unique_ptr<RmRecord> records[join_buffer_size];
    int pos = 0;
    // 用来保存记录数
    int len = 0;

    void push(std::unique_ptr<RmRecord> r) {
      assert(!isFull());
      records[len++] = std::move(r); 
    }

    // std::unique_ptr<RmRecord> getNext() {
    //   if (pos < records.size()) {
    //       return std::move(records[pos++]);
    //   }
    //   return nullptr;
    // }

RmRecord* getRecord(){
    if (records[pos]) {
        return records[pos].get();
    }
    return nullptr;
}

    bool isFull() {
      return len >= join_buffer_size;
    }

    bool isEnd() {
      return pos >= len;
    }

    void reset() {
        for (auto& record : records) {
            record.reset();
        }
        pos = 0;
        len = 0;
    }

};

