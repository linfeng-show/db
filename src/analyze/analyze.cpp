/* Copyright (c) 2023 Renmin University of China
RMDB is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

#include "analyze.h"

/**
 * @description: 分析器，进行语义分析和查询重写，需要检查不符合语义规定的部分
 * @param {shared_ptr<ast::TreeNode>} parse parser生成的结果集
 * @return {shared_ptr<Query>} Query 
 */
std::shared_ptr<Query> Analyze::do_analyze(std::shared_ptr<ast::TreeNode> parse)
{
    std::shared_ptr<Query> query = std::make_shared<Query>();
    if (auto x = std::dynamic_pointer_cast<ast::SelectStmt>(parse))
    {
        // 处理表名
        query->tables = std::move(x->tabs);
        /** TODO: 检查表是否存在 */

        // 07/05 检查表是否存在
        for (auto qtb : query->tables) {
        // 此处调用sm_manager里面的自带方法，差点忘了
            if(!sm_manager_->db_.is_table(qtb)) {
                throw TableNotFoundError(qtb);
            }
        }
        
       // 处理target list，再target list中添加上表名，例如 a.id
         for (auto &xcol : x->cols) {
            if(xcol->col_name.empty()){
                std::vector<ColMeta> col;
                get_all_cols(query->tables, col);
                TabCol sel_col = {.tab_name = xcol->tab_name, .col_name = col[0].name ,.JHName=xcol->JHName , .newcolname=xcol->newcolname,};
                query->cols.push_back(sel_col);
            }else{
            TabCol tab_col = {.tab_name = xcol->tab_name, .col_name = xcol->col_name, .JHName=xcol->JHName , .newcolname=xcol->newcolname,};
            query->cols.push_back(tab_col);
            }
        }
        
        std::vector<ColMeta> cols;
        get_all_cols(query->tables, cols);
        if (query->cols.empty()) {
            // select all columns
            for (auto &col : cols) {
                TabCol sel_col = {.tab_name = col.tab_name, .col_name = col.name};
                query->cols.push_back(sel_col);
            }
        } else {
            // infer table name from column name
            for (auto &sel_col : query->cols) {
                sel_col = check_column(cols, sel_col);  // 列元数据校验
            }
        }
        //处理where条件
        get_clause(x->conds, query->conds);
        check_clause(query->tables, query->conds);
    } else if (auto x = std::dynamic_pointer_cast<ast::UpdateStmt>(parse)) {
        /** TODO: */
        // 07/05 此处是update操作的where条件分析 
      TabMeta &tab = sm_manager_->db_.get_table(x->tab_name);

        for (auto &sv_set_clause : x->set_clauses) {
            if(!sv_set_clause->col_name1.empty()){
                auto glhs_col = tab.get_col(sv_set_clause->col_name);
                if(glhs_col->type==TYPE_STRING||glhs_col->type==TYPE_DATETIME)
                throw InternalError(glhs_col->name+" not can + or - ");
                if(sv_set_clause->col_name1!=sv_set_clause->col_name)
                throw InternalError(sv_set_clause->col_name+" " + sv_set_clause->col_name1+" not same");
                SetClause set_clause = {.lhs = {.tab_name = x->tab_name, .col_name = sv_set_clause->col_name},.rhs = convert_sv_value(sv_set_clause->val),.cpy=1,.si=sv_set_clause->si};
                query->set_clauses.push_back(set_clause);
            }
            else{
            SetClause set_clause = {.lhs = {.tab_name = x->tab_name, .col_name = sv_set_clause->col_name},.rhs = convert_sv_value(sv_set_clause->val)};
            query->set_clauses.push_back(set_clause);
            }
        }
        
        for (auto &set_clause : query->set_clauses) {
            auto lhs_col = tab.get_col(set_clause.lhs.col_name);
            // 写的不错
            if (lhs_col->type != set_clause.rhs.type) {
                // 测试过程中有个问题 类型不匹配可能是float和int型，但根据mysql，这样其实也是正确的条件
                // 尝试强转，但失败了，不如写一个类型转换器，直接赋值，也方便之后用，就是下面这样
                
                // 自拟转换器方法
                set_clause.rhs.type = convertToType(set_clause.rhs, lhs_col->type);

                if(lhs_col->type != set_clause.rhs.type){// 通过转换器 还不同，则直接抛出
                     throw IncompatibleTypeError(coltype2str(lhs_col->type), coltype2str(set_clause.rhs.type));
                }

                //直接转换
                // std::cout<<"类型"<<std::endl;
                // std::cout<<set_clause.rhs.type<<" "<< lhs_col->type<<std::endl;
                
                // if(set_clause.rhs.type == TYPE_INT && lhs_col->type == TYPE_FLOAT){
                //     set_clause.rhs.set_float(set_clause.rhs.float_val);
                // }else{
                //     throw IncompatibleTypeError(coltype2str(lhs_col->type), coltype2str(set_clause.rhs.type));
                // }  
               
            }
            set_clause.rhs.init_raw(lhs_col->len);
        }
        //处理where条件
        get_clause(x->conds,query->conds);
        check_clause({x->tab_name}, query->conds);
    // 这段代码后续会做修改 比较麻烦

    } else if (auto x = std::dynamic_pointer_cast<ast::DeleteStmt>(parse)) {
        //处理where条件
        get_clause(x->conds, query->conds);
        check_clause({x->tab_name}, query->conds);        
    } else if (auto x = std::dynamic_pointer_cast<ast::InsertStmt>(parse)) {
        // 处理insert 的values值
        for (auto &sv_val : x->vals) {
            query->values.push_back(convert_sv_value(sv_val));
        }
    } 
    // else if (auto x = std::dynamic_pointer_cast<ast::LoadStmt>(parse)) {
    //     // do nothing

    // }
    else {
        // do nothing
    }
    query->parse = std::move(parse);
    return query;
}


TabCol Analyze::check_column(const std::vector<ColMeta> &all_cols, TabCol target) {
    if (target.tab_name.empty()) {
        // Table name not specified, infer table name from column name
        std::string tab_name;
        for (auto &col : all_cols) {
            if (col.name == target.col_name) {
                if((target.JHName==4 && col.type==TYPE_STRING)||(target.JHName==4 && col.type==TYPE_DATETIME)){
                    throw InternalError("the type can not sum");
                }
                if (!tab_name.empty()) {
                    throw AmbiguousColumnError(target.col_name);
                }
                tab_name = col.tab_name;
            }
        }
        if (tab_name.empty()) {
            throw ColumnNotFoundError(target.col_name);
        }
        target.tab_name = tab_name;
    } else {
        /** TODO: Make sure target column exists */

        // 0705 排错 此处检查目标表是否存在col_name这一列
        if (!(sm_manager_->db_.get_table(target.tab_name).is_col(target.col_name))) {
            
            throw ColumnNotFoundError(target.col_name);
        }
        // 
    }
    return target;
}

void Analyze::get_all_cols(const std::vector<std::string> &tab_names, std::vector<ColMeta> &all_cols) {
    for (auto &sel_tab_name : tab_names) {
        // 这里db_不能写成get_db(), 注意要传指针
        const auto &sel_tab_cols = sm_manager_->db_.get_table(sel_tab_name).cols;
        all_cols.insert(all_cols.end(), sel_tab_cols.begin(), sel_tab_cols.end());
    }
}

void Analyze::get_clause(const std::vector<std::shared_ptr<ast::BinaryExpr>> &sv_conds, std::vector<Condition> &conds) {
    conds.clear();
    for (auto &expr : sv_conds) {
        Condition cond;
        cond.lhs_col = {.tab_name = expr->lhs->tab_name, .col_name = expr->lhs->col_name};
        cond.op = convert_sv_comp_op(expr->op);
        if (auto rhs_val = std::dynamic_pointer_cast<ast::Value>(expr->rhs)) {
            cond.is_rhs_val = true;
            cond.rhs_val = convert_sv_value(rhs_val);
        } else if (auto rhs_col = std::dynamic_pointer_cast<ast::Col>(expr->rhs)) {
            cond.is_rhs_val = false;
            cond.rhs_col = {.tab_name = rhs_col->tab_name, .col_name = rhs_col->col_name};
        }
        conds.push_back(cond);
    }
}

void Analyze::check_clause(const std::vector<std::string> &tab_names, std::vector<Condition> &conds) {
    // auto all_cols = get_all_cols(tab_names);
    std::vector<ColMeta> all_cols;
    get_all_cols(tab_names, all_cols);
    // Get raw values in where clause
    for (auto &cond : conds) {
        // Infer table name from column name
        cond.lhs_col = check_column(all_cols, cond.lhs_col);
        if (!cond.is_rhs_val) {
            cond.rhs_col = check_column(all_cols, cond.rhs_col);
        }
        TabMeta &lhs_tab = sm_manager_->db_.get_table(cond.lhs_col.tab_name);
        auto lhs_col = lhs_tab.get_col(cond.lhs_col.col_name);
        ColType lhs_type = lhs_col->type;
        ColType rhs_type;
        if (cond.is_rhs_val) {

            // 07/05 修改此处代码，使int和float可以进行比较
            // 如果右值是int,并且左值是float
            // 就将右值自动转为float
            if (cond.rhs_val.type == TYPE_INT && lhs_type == TYPE_FLOAT) {
                cond.rhs_val.type = TYPE_FLOAT;
                cond.rhs_val.float_val = cond.rhs_val.int_val;
            }
            // 左值int 右值float
            if (cond.rhs_val.type == TYPE_FLOAT && lhs_type == TYPE_INT) {
                lhs_type = TYPE_FLOAT;
            }

            // 07/11 如果int型给bigint型赋值，则自动转换int为bigint
            if(cond.rhs_val.type == TYPE_INT && lhs_type == TYPE_BIGINT){
                cond.rhs_val.type = TYPE_BIGINT;
                cond.rhs_val.bigint_val = cond.rhs_val.int_val;
            }

            // 此处如果右值为date左值为char，则兼容转换
            if(cond.rhs_val.type == TYPE_DATETIME && lhs_type == TYPE_STRING){
                cond.rhs_val.type = TYPE_STRING;
               cond.rhs_val.str_val = cond.rhs_val.datetime.ToString();
            }
            // 以上
            cond.rhs_val.init_raw(lhs_col->len);
            rhs_type = cond.rhs_val.type;
        } else {
            TabMeta &rhs_tab = sm_manager_->db_.get_table(cond.rhs_col.tab_name);
            auto rhs_col = rhs_tab.get_col(cond.rhs_col.col_name);
            rhs_type = rhs_col->type;
        }
        if (lhs_type != rhs_type) {
                // 以下作废，无法在此处转换，仅作纪念
                
                // if(rhs_type == TYPE_INT && lhs_type == TYPE_FLOAT){
                // //    这样可以执行
                // }else{
                //     throw IncompatibleTypeError(coltype2str(lhs_type), coltype2str(rhs_type));
                // }  

             throw IncompatibleTypeError(coltype2str(lhs_type), coltype2str(rhs_type));
        }
    }
}


Value Analyze::convert_sv_value(const std::shared_ptr<ast::Value> &sv_val) {
    Value val;
    if (auto int_lit = std::dynamic_pointer_cast<ast::IntLit>(sv_val)) {
        val.set_int(int_lit->val);
    } else if (auto float_lit = std::dynamic_pointer_cast<ast::FloatLit>(sv_val)) {
        val.set_float(float_lit->val);
    } else if (auto str_lit = std::dynamic_pointer_cast<ast::StringLit>(sv_val)) {
        val.set_str(str_lit->val);
    } else if (auto bigint_lit = std::dynamic_pointer_cast<ast::BigintLit>(sv_val)) {
        val.set_bigint(bigint_lit->val);
    } else if (auto datetime_lit = std::dynamic_pointer_cast<ast::DatetimeLit>(sv_val)) {
        val.set_datetime(datetime_lit->val); 
    }else {
        throw InternalError("Unexpected sv value type");
    }
    return val;
}

CompOp Analyze::convert_sv_comp_op(ast::SvCompOp op) {
    std::map<ast::SvCompOp, CompOp> m = {
        {ast::SV_OP_EQ, OP_EQ}, {ast::SV_OP_NE, OP_NE}, {ast::SV_OP_LT, OP_LT},
        {ast::SV_OP_GT, OP_GT}, {ast::SV_OP_LE, OP_LE}, {ast::SV_OP_GE, OP_GE},
    };
    return m.at(op);
}


