%{
#include "ast.h"
#include "yacc.tab.h"
#include <iostream>
#include <memory>

int yylex(YYSTYPE *yylval, YYLTYPE *yylloc);

void yyerror(YYLTYPE *locp, const char* s) {
    std::cerr << "Parser Error at line " << locp->first_line << " column " << locp->first_column << ": " << s << std::endl;
}

using namespace ast;
%}

// request a pure (reentrant) parser
%define api.pure full
// enable location in error handler
%locations
// enable verbose syntax error message
%define parse.error verbose

// keywords
%token SHOW TABLES CREATE TABLE DROP DESC INSERT INTO VALUES DELETE FROM ASC ORDER BY
WHERE UPDATE SET SELECT INT CHAR FLOAT BIGINT DATETIME INDEX AND JOIN EXIT HELP TXN_BEGIN TXN_COMMIT TXN_ABORT TXN_ROLLBACK ORDER_BY AS COUNT MIN MAX SUM LIMIT
LOAD OUTPUT_FILE PATH BACK
// non-keywords
%token LEQ NEQ GEQ T_EOF ADD SUB

// type-specific tokens
%token <sv_str> IDENTIFIER VALUE_STRING
%token <sv_int> VALUE_INT
%token <sv_float> VALUE_FLOAT
%token <sv_str> VALUE_BIGINT
%token <sv_str> VALUE_DATETIME

// specify types for non-terminal symbol
%type <sv_node> stmt dbStmt ddl dml txnStmt loadStmt
%type <sv_field> field
%type <sv_fields> fieldList
%type <sv_type_len> type
%type <sv_comp_op> op
%type <sv_expr> expr
%type <sv_val> value
%type <sv_vals> valueList
%type <sv_str> tbName colName newname file_name is_out
%type <sv_strs> tableList colNameList
%type <sv_col> col cols
%type <sv_cols> colList selector
%type <sv_set_clause> setClause
%type <sv_set_clauses> setClauses
%type <sv_cond> condition
%type <sv_conds> whereClause optWhereClause
%type <sv_orderby>  order_clause
%type <sv_orderbys> order_list
%type <op_sv_orderbys> opt_order
%type <sv_orderby_dir> opt_asc_desc

/* */

%%
start:
        stmt ';'
    {
        parse_tree = $1;
        YYACCEPT;
    }
    |   HELP
    {
        parse_tree = std::make_shared<Help>();
        YYACCEPT;
    }
    |   EXIT
    {
        parse_tree = nullptr;
        YYACCEPT;
    }
    |   T_EOF
    {
        parse_tree = nullptr;
        YYACCEPT;
    }
    |   SET OUTPUT_FILE is_out
    {
        parse_tree = std::make_shared<Outfile>($3);
        YYACCEPT;
    }
    ;

stmt:
        dbStmt
    |   ddl
    |   dml
    |   txnStmt
    |   loadStmt
    ;

txnStmt:
        TXN_BEGIN
    {
        $$ = std::make_shared<TxnBegin>();
    }
    |   TXN_COMMIT
    {
        $$ = std::make_shared<TxnCommit>();
    }
    |   TXN_ABORT
    {
        $$ = std::make_shared<TxnAbort>();
    }
    | TXN_ROLLBACK
    {
        $$ = std::make_shared<TxnRollback>();
    }
    ;

dbStmt:
        SHOW TABLES
    {
        $$ = std::make_shared<ShowTables>();
    }
    ;
    |
        SHOW INDEX FROM tbName
    {
        $$ = std::make_shared<ShowIndex>($4);
    }
    ;

loadStmt:
        LOAD PATH file_name BACK INTO tbName
    {
        $$ = std::make_shared<LoadStmt>($3, $6);
    }
    ;

ddl:
        CREATE TABLE tbName '(' fieldList ')'
    {
        $$ = std::make_shared<CreateTable>($3, $5);
    }
    |   DROP TABLE tbName
    {
        $$ = std::make_shared<DropTable>($3);
    }
    |   DESC tbName
    {
        $$ = std::make_shared<DescTable>($2);
    }
    |   CREATE INDEX tbName '(' colNameList ')'
    {
        $$ = std::make_shared<CreateIndex>($3, $5);
    }
    |   DROP INDEX tbName '(' colNameList ')'
    {
        $$ = std::make_shared<DropIndex>($3, $5);
    }
    ;

dml:
        INSERT INTO tbName VALUES '(' valueList ')'
    {
        $$ = std::make_shared<InsertStmt>($3, $6);
    }
    |   DELETE FROM tbName optWhereClause
    {
        $$ = std::make_shared<DeleteStmt>($3, $4);
    }
    |   UPDATE tbName SET setClauses optWhereClause
    {
        $$ = std::make_shared<UpdateStmt>($2, $4, $5);
    }
    |   SELECT selector FROM tableList optWhereClause opt_order
    {
        $$ = std::make_shared<SelectStmt>($2, $4, $5, $6);
    }
    ;

fieldList:
        field
    {
        $$ = std::vector<std::shared_ptr<Field>>{$1};
    }
    |   fieldList ',' field
    {
        $$.push_back($3);
    }
    ;

colNameList:
        colName
    {
        $$ = std::vector<std::string>{$1};
    }
    | colNameList ',' colName
    {
        $$.push_back($3);
    }
    ;

field:
        colName type
    {
        $$ = std::make_shared<ColDef>($1, $2);
    }
    ;

type:
        INT
    {
        $$ = std::make_shared<TypeLen>(SV_TYPE_INT, sizeof(int));
    }
    |   CHAR '(' VALUE_INT ')'
    {
        $$ = std::make_shared<TypeLen>(SV_TYPE_STRING, $3);
    }
    |   FLOAT
    {
        $$ = std::make_shared<TypeLen>(SV_TYPE_FLOAT, sizeof(float));
    }
    |   BIGINT
    {
        $$ = std::make_shared<TypeLen>(SV_TYPE_BIGINT, sizeof(int64_t));
    }
    |   DATETIME
    {
        $$ = std::make_shared<TypeLen>(SV_TYPE_DATETIME, sizeof(uint64_t));
    }
    ;

valueList:
        value
    {
        $$ = std::vector<std::shared_ptr<Value>>{$1};
    }
    |   valueList ',' value
    {
        $$.push_back($3);
    }
    ;

value:
        VALUE_INT
    {
        $$ = std::make_shared<IntLit>($1);
    }
    |   VALUE_FLOAT
    {
        $$ = std::make_shared<FloatLit>($1);
    }
    |   VALUE_STRING
    {
        $$ = std::make_shared<StringLit>($1);
    }
    |   VALUE_BIGINT
    {
        $$ = std::make_shared<BigintLit>($1);
    }
    |   VALUE_DATETIME
    {
        $$ = std::make_shared<DatetimeLit>($1);
    }
    ;

condition:
        col op expr
    {
        $$ = std::make_shared<BinaryExpr>($1, $2, $3);
    }
    ;

optWhereClause:
        /* epsilon */ { /* ignore*/ }
    |   WHERE whereClause
    {
        $$ = $2;
    }
    ;

whereClause:
        condition 
    {
        $$ = std::vector<std::shared_ptr<BinaryExpr>>{$1};
    }
    |   whereClause AND condition
    {
        $$.push_back($3);
    }
    ;

col:
        tbName '.' colName
    {
        $$ = std::make_shared<Col>($1, $3);
    }
    |   colName
    {
        $$ = std::make_shared<Col>("", $1);
    }
    ;

cols:
        COUNT '(' colName ')' AS newname
    {
        $$ = std::make_shared<Col>("",$3,1,$6);
    }
    |   COUNT '(' tbName '.' colName ')' AS newname
    {
        $$ = std::make_shared<Col>($3,$5,1,$8);
    }
    |   COUNT '(' '*' ')' AS newname
    {
        $$ = std::make_shared<Col>("","",1,$6);
    }
    |   MIN '(' colName ')' AS newname
    {
        $$ = std::make_shared<Col>("",$3,2,$6);
    }
    |   MIN '(' tbName '.' colName ')' AS newname
    {
        $$ = std::make_shared<Col>($3,$5,2,$8);
    }
    |   MAX '(' colName ')' AS newname
    {
        $$ = std::make_shared<Col>("",$3,3,$6);
    }
    |   MAX '(' tbName '.' colName ')' AS newname
    {
        $$ = std::make_shared<Col>($3,$5,3,$8);
    }
    |   SUM '(' colName ')' AS newname
    {
        $$ = std::make_shared<Col>("",$3,4,$6);
    }
    |   SUM '(' tbName '.' colName ')' AS newname
    {
        $$ = std::make_shared<Col>($3,$5,4,$8);
    }
    ;

colList:
        col
    {
        $$ = std::vector<std::shared_ptr<Col>>{$1};
    }
    |   colList ',' col
    {
        $$.push_back($3);
    }
    ;

op:
        '='
    {
        $$ = SV_OP_EQ;
    }
    |   '<'
    {
        $$ = SV_OP_LT;
    }
    |   '>'
    {
        $$ = SV_OP_GT;
    }
    |   NEQ
    {
        $$ = SV_OP_NE;
    }
    |   LEQ
    {
        $$ = SV_OP_LE;
    }
    |   GEQ
    {
        $$ = SV_OP_GE;
    }
    ;

expr:
        value
    {
        $$ = std::static_pointer_cast<Expr>($1);
    }
    |   col
    {
        $$ = std::static_pointer_cast<Expr>($1);
    }
    ;

setClauses:
        setClause
    {
        $$ = std::vector<std::shared_ptr<SetClause>>{$1};
    }
    |   setClauses ',' setClause
    {
        $$.push_back($3);
    }
    ;

setClause:
        colName '=' value
    {
        $$ = std::make_shared<SetClause>($1, $3);
    }
    |   colName '=' colName ADD value
    {
        $$ = std::make_shared<SetClause>($1, $3, 1 , $5);
    }
    |   colName '=' colName SUB value
    {
        $$ = std::make_shared<SetClause>($1, $3, 2 , $5);
    }
    |   colName '=' value ADD colName
    {
        $$ = std::make_shared<SetClause>($1, $5, 1 , $3);
    }
    |   colName '=' value SUB colName
    {
        $$ = std::make_shared<SetClause>($1, $5, 3 , $3);
    }
    |   colName '=' colName value
    {
        $$ = std::make_shared<SetClause>($1, $3, 1 , $4);
    }
    ;

selector:
        '*'
    {
        $$ = {};
    }
    |   cols
    {
        $$ = std::vector<std::shared_ptr<Col>>{$1};
    }
    |   colList
    ;

tableList:
        tbName
    {
        $$ = std::vector<std::string>{$1};
    }
    |   tableList ',' tbName
    {
        $$.push_back($3);
    }
    |   tableList JOIN tbName
    {
        $$.push_back($3);
    }
    ;

opt_order:

    ORDER BY order_list
    {
        $$ = std::pair<std::vector<std::shared_ptr<OrderBy>>, int>{$3, -1};
    }
    |
    ORDER BY order_list LIMIT VALUE_INT
    {
        $$ = std::pair<std::vector<std::shared_ptr<OrderBy>>, int>{$3, $5};
    }
    | /* epsilon */ { /* ignore*/ }
    ;

order_list:
    order_clause      
    { 
        $$ = std::vector<std::shared_ptr<OrderBy>>{$1}; 
    }
    |
    order_list ',' order_clause
    {
        $$.push_back($3);
    }
    ;

order_clause:
      col  opt_asc_desc 
    { 
        $$ = std::make_shared<OrderBy>($1, $2);
    }
    ;     


opt_asc_desc:
    ASC          { $$ = OrderBy_ASC;     }
    |  DESC      { $$ = OrderBy_DESC;    }
    |       { $$ = OrderBy_DEFAULT; }
    ;    

file_name: IDENTIFIER;

is_out: IDENTIFIER;

tbName: IDENTIFIER;

colName: IDENTIFIER;

newname:IDENTIFIER;
%%
