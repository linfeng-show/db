/* Copyright (c) 2023 Renmin University of China
RMDB is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

#include "lock_manager.h"

/**
 * @description: 申请行级共享锁
 * @return {bool} 加锁是否成功
 * @param {Transaction*} txn 要申请锁的事务对象指针
 * @param {Rid&} rid 加锁的目标记录ID 记录所在的表的fd
 * @param {int} tab_fd
 */
bool LockManager::lock_shared_on_record(Transaction* txn, const Rid& rid, int tab_fd) {
    //std::cout<<txn->get_transaction_id()<<"lock_shared_on_record"<<std::endl;

    // 访问全局锁表
    std::unique_lock<std::mutex> lock(latch_);

    if (txn->get_state() == TransactionState::ABORTED) {
        return false;
    }

    auto lock_data_id = LockDataId(tab_fd, rid, LockDataType::RECORD);
    auto lock_data = lock_table_.find(lock_data_id);
    
    if (lock_data != lock_table_.end()) {
        auto request_queue = lock_data->second.request_queue_;

        for(auto &request : request_queue) {


            // 当前事务在请求队列中有锁，不管是X还是S都放行
            if (request.txn_id_ == txn->get_transaction_id()) {
                return true;
            }
            // // 其他事务上存在X锁
            // if (request.txn_id_ != txn->get_transaction_id() && request.lock_mode_ == LockMode::EXLUCSIVE) {
            //     throw TransactionAbortException(txn->get_transaction_id(), AbortReason::DEADLOCK_PREVENTION);
            // }
            // return true;
        }
        // return true;
    }

    LockRequest request(txn->get_transaction_id(), LockMode::SHARED);
    txn->set_state(TransactionState::GROWING);

    if (lock_data == lock_table_.end()) {
        txn->get_lock_set()->emplace(lock_data_id); // 加入锁集
        lock_table_[lock_data_id].request_queue_.push_back(request);
        lock_table_[lock_data_id].group_lock_mode_ = GroupLockMode::S;
        
        return true;
    }

    else {
        // std::cout<<"当前mode: "<<lock_data->second.group_lock_mode_<<std::endl;
        // S锁应该只有空锁是兼容的
        if (lock_data->second.group_lock_mode_ == GroupLockMode::NON_LOCK) { // 处理S锁
            txn->get_lock_set()->emplace(lock_data_id);
            request.granted_ = true;
            lock_data->second.request_queue_.push_back(request);
            lock_data->second.group_lock_mode_ = GroupLockMode::S;
            
            return true;
        }
        if (lock_data->second.group_lock_mode_ == GroupLockMode::S) {
            txn->get_lock_set()->emplace(lock_data_id);
            request.granted_ = true;
            lock_data->second.request_queue_.push_back(request);
            
            return true;
        }
        
        // txn->set_state(TransactionState::ABORTED);  // 回滚
       throw TransactionAbortException(txn->get_transaction_id(), AbortReason::DEADLOCK_PREVENTION);
    }
    return false;
}

/**
 * @description: 申请行级排他锁
 * @return {bool} 加锁是否成功
 * @param {Transaction*} txn 要申请锁的事务对象指针
 * @param {Rid&} rid 加锁的目标记录ID
 * @param {int} tab_fd 记录所在的表的fd
 */
bool LockManager::lock_exclusive_on_record(Transaction* txn, const Rid& rid, int tab_fd) {
   // std::cout<<txn->get_transaction_id()<<"lock_exclusive_on_record"<<std::endl;


    std::unique_lock<std::mutex> lock(latch_);

    if (txn->get_state() == TransactionState::ABORTED) {
        return false;
    }

    auto lock_data_id = LockDataId(tab_fd, rid, LockDataType::RECORD);
    auto lock_data = lock_table_.find(lock_data_id);
    if (lock_data != lock_table_.end()) {
        auto request_queue = lock_data->second.request_queue_;
        for(auto &request : request_queue) {
            if (request.txn_id_ == txn->get_transaction_id()) {

                if (request.lock_mode_ == LockMode::EXLUCSIVE) {
                    return true;
                }
                if (request.lock_mode_ == LockMode::SHARED) {

                    // 修改：有且只有当前事务有S锁在该资源请求队列内，才能升级，不能直接升级
                    // 这里升级X锁可能会和其他事务的S锁冲突

                    if(request_queue.size() == 1){
                        // 有S锁，升级为X锁
                        // TODO: 判断是否有同时升级的
                        request.lock_mode_ = LockMode::EXLUCSIVE;
                        lock_data->second.group_lock_mode_ = GroupLockMode::X;

                        return true;
                    }
                    throw TransactionAbortException(txn->get_transaction_id(), AbortReason::DEADLOCK_PREVENTION);
                }

                // throw TransactionAbortException(txn->get_transaction_id(), AbortReason::DEADLOCK_PREVENTION);
            }
        }
    }


    LockRequest request(txn->get_transaction_id(), LockMode::EXLUCSIVE);
    txn->set_state(TransactionState::GROWING);


    if (lock_data == lock_table_.end()) {
        txn->get_lock_set()->emplace(lock_data_id); // 加入锁集
        request.granted_ = true;
        lock_table_[lock_data_id].request_queue_.push_back(request);
        lock_table_[lock_data_id].group_lock_mode_ = GroupLockMode::X;
        
        return true;
    }
    else {
        // std::cout<<"mode: "<<lock_data->second.group_lock_mode_<<std::endl;
        // 处理兼容 X锁只能加在空锁上，其余抛错
        if (lock_data->second.group_lock_mode_ == GroupLockMode::NON_LOCK) {
            txn->get_lock_set()->emplace(lock_data_id);
            request.granted_ = true;
            lock_data->second.request_queue_.push_back(request);
            lock_data->second.group_lock_mode_ = GroupLockMode::X;
            
            return true;
        }

        throw TransactionAbortException(txn->get_transaction_id(), AbortReason::DEADLOCK_PREVENTION);
    }
    return false;
}

/**
 * @description: 申请表级读锁
 * @return {bool} 返回加锁是否成功
 * @param {Transaction*} txn 要申请锁的事务对象指针
 * @param {int} tab_fd 目标表的fd
 */
bool LockManager::lock_shared_on_table(Transaction* txn, int tab_fd) {
   // std::cout<<"lock_shared_on_table"<<std::endl;
    
    // 访问全局表锁
    std::unique_lock<std::mutex> lock(latch_);

    if (txn->get_state() == TransactionState::ABORTED) {
        // 如果是abort了，无法加锁
        return false;
    }
    // 检查全局锁表中该表已存在的锁
    auto lockdata_id = LockDataId(tab_fd,LockDataType::TABLE);
    auto lock_data = lock_table_.find(lockdata_id);
    if(lock_data != lock_table_.end()){
        // 说明全局表锁里当前表存在锁,取加锁队列遍历看锁对不对
        auto queue = lock_data->second.request_queue_;
        for(auto q : queue){
            if(q.txn_id_ == txn->get_transaction_id()){
                if(q.lock_mode_ == LockMode::SHARED || q.lock_mode_ == LockMode::EXLUCSIVE || q.lock_mode_ == LockMode::S_IX){
                    // 同一事务应该放行读操作
                    return true;
                }else if (q.lock_mode_ == LockMode::INTENTION_EXCLUSIVE) {
                    // 升级IX锁为S_IX锁
                    q.lock_mode_ = LockMode::S_IX;
                    return true;
                }
                throw TransactionAbortException(txn->get_transaction_id(), AbortReason::DEADLOCK_PREVENTION);
            }
        }
    }

    // 不存在共享锁，继续
    LockRequest lr(txn->get_transaction_id(), LockMode::SHARED); //构建锁
    txn->set_state(TransactionState::GROWING); 
    if(lock_data == lock_table_.end()){ // 第一次加锁的情况，直接加
        txn->get_lock_set()->emplace(lockdata_id);
        lr.granted_ = true;
        lock_table_[lockdata_id].request_queue_.push_back(lr);
        lock_table_[lockdata_id].group_lock_mode_ = GroupLockMode::S;

        // 忘记return了
        return true;
    }else{
        // 处理兼容,S锁兼容IS,空锁
        // std::cout<<"mode: "<<lock_data->second.group_lock_mode_<<std::endl; 
        switch(lock_data->second.group_lock_mode_){
            case GroupLockMode::S:{
                // S锁
                txn->get_lock_set()->emplace(lockdata_id);
                lr.granted_ = true;
                lock_data->second.request_queue_.push_back(lr);
                return true;
                 
            }
            case GroupLockMode::IS:{
                // IS锁
                txn->get_lock_set()->emplace(lockdata_id);
                lr.granted_ = true;
                lock_data->second.request_queue_.push_back(lr);
                lock_data->second.group_lock_mode_ = GroupLockMode::S;
                return true;
            }
            case GroupLockMode::NON_LOCK:{
                txn->get_lock_set()->emplace(lockdata_id);
                // 非锁
                lr.granted_ = true;
                lock_data->second.request_queue_.push_back(lr);
                lock_data->second.group_lock_mode_ = GroupLockMode::S;
                return true;
            }
            default :
                throw TransactionAbortException(txn->get_transaction_id(), AbortReason::DEADLOCK_PREVENTION);
        }

    }

    return false;  
}

/**
 * @description: 申请表级写锁
 * @return {bool} 返回加锁是否成功
 * @param {Transaction*} txn 要申请锁的事务对象指针
 * @param {int} tab_fd 目标表的fd
 */
bool LockManager::lock_exclusive_on_table(Transaction* txn, int tab_fd) {
    //std::cout<<"lock_exclusive_on_table"<<std::endl;
     // 访问全局表锁
    std::unique_lock<std::mutex> lock(latch_);

    if (txn->get_state() == TransactionState::ABORTED) {
        // 如果是abort了，无法加锁
        return false;
    }
    // 检查全局锁表中该表已存在的锁
    auto lockdata_id = LockDataId(tab_fd,LockDataType::TABLE);
    auto lock_data = lock_table_.find(lockdata_id);
    if(lock_data != lock_table_.end()){
        // 说明全局表锁里当前表存在锁,取加锁队列遍历看锁对不对
        auto queue = lock_data->second.request_queue_;
        for(auto &q : queue){
            if(q.txn_id_ == txn->get_transaction_id()){
                if(lock_data->second.request_queue_.size() == 1){ // 请求队列中只存在当前事务的加锁请求，才能升级
                    q.lock_mode_ = LockMode::EXLUCSIVE;
                    lock_data->second.group_lock_mode_ = GroupLockMode::X;
                    return true;
                }
                if (q.lock_mode_ == LockMode::EXLUCSIVE) {
                    return true;
                }
                throw TransactionAbortException(txn->get_transaction_id(), AbortReason::DEADLOCK_PREVENTION);
            }
        }
    }
    LockRequest lr(txn->get_transaction_id(), LockMode::EXLUCSIVE); //构建锁
    txn->set_state(TransactionState::GROWING);
    if(lock_data == lock_table_.end()){ // 第一次加锁的情况，直接加
        txn->get_lock_set()->emplace(lockdata_id);
        lr.granted_ = true;
        lock_table_[lockdata_id].request_queue_.push_back(lr);
        lock_table_[lockdata_id].group_lock_mode_ = GroupLockMode::S;

        // 忘记return了
        return true;
    }else{
        // std::cout<<"mode: "<<lock_data->second.group_lock_mode_<<std::endl;
       // 兼容锁处理 X锁只兼容空锁
        if (lock_data->second.group_lock_mode_ == GroupLockMode::NON_LOCK) {
            txn->get_lock_set()->emplace(lockdata_id);
            lr.granted_ = true;
            lock_data->second.request_queue_.push_back(lr);
            lock_data->second.group_lock_mode_ = GroupLockMode::X;
            return true;
        }
        // 抛错
        throw TransactionAbortException(txn->get_transaction_id(), AbortReason::DEADLOCK_PREVENTION);
    }
    
    return false;
}

/**
 * @description: 申请表级意向读锁
 * @return {bool} 返回加锁是否成功
 * @param {Transaction*} txn 要申请锁的事务对象指针
 * @param {int} tab_fd 目标表的fd
 */
bool LockManager::lock_IS_on_table(Transaction* txn, int tab_fd) {
   // std::cout<<"lock_IS_on_table"<<std::endl;
    // 访问全局表锁
    std::unique_lock<std::mutex> lock(latch_);

    if (txn->get_state() == TransactionState::ABORTED) {
        // 如果是abort了，无法加锁
        return false;
    }
    // 检查全局锁表中该表已存在的锁
    auto lockdata_id = LockDataId(tab_fd,LockDataType::TABLE);
    auto lock_data = lock_table_.find(lockdata_id);
    if(lock_data != lock_table_.end()){
        // 说明全局表锁里当前表存在锁,取加锁队列遍历看锁对不对
        auto queue = lock_data->second.request_queue_;
        for(auto q : queue){
            if(q.txn_id_ == txn->get_transaction_id()){
                // if(q.lock_mode_ == LockMode::INTENTION_SHARED){
                //     return true;
                // }
                // if(q.lock_mode_ == LockMode::SHARED){
                //     q.lock_mode_ = LockMode::INTENTION_SHARED;
                //     return true;
                // }
                // else{
                //     return false;
                // }
                return true;
            }
        }
    }
    LockRequest lr(txn->get_transaction_id(), LockMode::INTENTION_SHARED); //构建锁
    txn->set_state(TransactionState::GROWING);
    if(lock_data == lock_table_.end()){ // 第一次加锁的情况，直接加
        txn->get_lock_set()->emplace(lockdata_id);
        lr.granted_ = true;
        lock_table_[lockdata_id].request_queue_.push_back(lr);
        lock_table_[lockdata_id].group_lock_mode_ = GroupLockMode::IS;

        // 忘记return了
        return true;
    }else{
        // std::cout<<"mode: "<<lock_data->second.group_lock_mode_<<std::endl;
        // 兼容锁处理:不兼容X锁
        if (lock_data->second.group_lock_mode_ == GroupLockMode::NON_LOCK) {
            txn->get_lock_set()->emplace(lockdata_id);
            lr.granted_ = true;
            lock_data->second.request_queue_.push_back(lr);
            lock_data->second.group_lock_mode_ = GroupLockMode::IS;
            return true;
        }
        if (lock_data->second.group_lock_mode_ == GroupLockMode::X){
            throw TransactionAbortException(txn->get_transaction_id(), AbortReason::DEADLOCK_PREVENTION);
        }
        // lr.granted_ = true;
        // lock_data->second.request_queue_.push_back(lr);
        // return true;
    }
    
    return false;
}

/**
 * @description: 申请表级意向写锁
 * @return {bool} 返回加锁是否成功
 * @param {Transaction*} txn 要申请锁的事务对象指针
 * @param {int} tab_fd 目标表的fd
 */
bool LockManager::lock_IX_on_table(Transaction* txn, int tab_fd) {
   // std::cout<<"lock_IX_on_table"<<std::endl;
    // 访问全局表锁
    std::unique_lock<std::mutex> lock(latch_);

    if (txn->get_state() == TransactionState::ABORTED) {
        // 如果是abort了，无法加锁
        return false;
    }
    // 检查全局锁表中该表已存在的锁
    auto lockdata_id = LockDataId(tab_fd,LockDataType::TABLE);
    auto lock_data = lock_table_.find(lockdata_id);
    if(lock_data != lock_table_.end()){
        // 说明全局表锁里当前表存在锁,取加锁队列遍历看锁对不对
        auto queue = lock_data->second.request_queue_;
        for(auto &q : queue){
            if(q.txn_id_ == txn->get_transaction_id()){
                if(q.lock_mode_ == LockMode::INTENTION_EXCLUSIVE){
                    return true;
                }
                if(q.lock_mode_ == LockMode::SHARED){
                    for(auto qu : queue){
                        if(qu.txn_id_ != txn->get_transaction_id() && qu.lock_mode_ == LockMode::SHARED){
                            throw TransactionAbortException(txn->get_transaction_id(), AbortReason::DEADLOCK_PREVENTION);
                        }
                    }
                    //std::cout<<"升级为SIX"<<std::endl;
                    q.lock_mode_ = LockMode::S_IX;
                    lock_data->second.group_lock_mode_ = GroupLockMode::SIX;
                    return true;
                }
                throw TransactionAbortException(txn->get_transaction_id(), AbortReason::DEADLOCK_PREVENTION);
            }
        }
    }
    LockRequest lr(txn->get_transaction_id(), LockMode::INTENTION_EXCLUSIVE); //构建锁
    txn->set_state(TransactionState::GROWING);
    if(lock_data == lock_table_.end()){ // 第一次加锁的情况，直接加
        txn->get_lock_set()->emplace(lockdata_id);
        lr.granted_ = true;
        lock_table_[lockdata_id].request_queue_.push_back(lr);
        lock_table_[lockdata_id].group_lock_mode_ = GroupLockMode::IX;

        // 忘记return了
        return true;
    }else{
        // std::cout<<"mode: "<<lock_data->second.group_lock_mode_<<std::endl;
        // 兼容锁处理 不兼容S,X
        if (lock_data->second.group_lock_mode_ == GroupLockMode::NON_LOCK) {
            txn->get_lock_set()->emplace(lockdata_id);
            lr.granted_ = true;
            lock_data->second.request_queue_.push_back(lr);
            lock_data->second.group_lock_mode_ = GroupLockMode::IX;
            return true;
        }
        if (lock_data->second.group_lock_mode_ == GroupLockMode::IX) {
            txn->get_lock_set()->emplace(lockdata_id);
            lr.granted_ = true;
            lock_data->second.request_queue_.push_back(lr);
            return true;
        }
        if (lock_data->second.group_lock_mode_ == GroupLockMode::IS) {
            txn->get_lock_set()->emplace(lockdata_id);
            lr.granted_ = true;
            lock_data->second.request_queue_.push_back(lr);
            lock_data->second.group_lock_mode_ = GroupLockMode::IX;
            return true;
        }
        throw TransactionAbortException(txn->get_transaction_id(), AbortReason::DEADLOCK_PREVENTION);
    }
    
    return false;
}

/**
 * @description: 释放锁
 * @return {bool} 返回解锁是否成功
 * @param {Transaction*} txn 要释放锁的事务对象指针
 * @param {LockDataId} lock_data_id 要释放的锁ID
 */
bool LockManager::unlock(Transaction* txn, LockDataId lock_data_id) {
    //std::cout<<"unlock"<<std::endl;
    
     //获取全局锁表
    std::unique_lock<std::mutex> lock(latch_);
    auto lock_data = lock_table_.find(lock_data_id);
   // std::cout<<"1"<<std::endl;
    if (lock_data == lock_table_.end()) {
        return false;
    }
    auto& request_queue = lock_data->second.request_queue_;
    for (auto request = request_queue.begin(); request != request_queue.end();) {
        if (request->txn_id_ == txn->get_transaction_id()) {
            request_queue.erase(request); // 清空加锁队列
            break;
        }else{
            ++request;
        }
    }
    //std::cout<<"2"<<std::endl;
    // lock_table_[lock_data_id].cv_.notify_one();
    // if (request_queue.empty()) {
    //     lock_table_.erase(lock_data_id); // 清空全局锁表
    //     // lock_data->second.group_lock_mode_ = GroupLockMode::NON_LOCK;
    // }
    //std::cout<<"3"<<std::endl;
    // ==================这里应该需要一个循环吧？===================
    // lock_data->second.group_lock_mode_ = GroupLockMode::NON_LOCK;
    // 判断组内的最强锁
    if (!request_queue.empty()) {
    GroupLockMode max_lock_mode = GroupLockMode::NON_LOCK;
    for (const auto& request : request_queue) {
        if (request.lock_mode_ == LockMode::EXLUCSIVE) {
            max_lock_mode = GroupLockMode::X;
            break;
        }else if (request.lock_mode_ == LockMode::S_IX && max_lock_mode < GroupLockMode::SIX){
            max_lock_mode = GroupLockMode::SIX;
        } else if (request.lock_mode_ == LockMode::INTENTION_EXCLUSIVE && max_lock_mode < GroupLockMode::IX) {
            max_lock_mode = GroupLockMode::IX;
        } else if (request.lock_mode_ == LockMode::SHARED && max_lock_mode < GroupLockMode::S) {
            max_lock_mode = GroupLockMode::S;
        } else if (request.lock_mode_ == LockMode::INTENTION_SHARED && max_lock_mode < GroupLockMode::IS) {
            max_lock_mode = GroupLockMode::IS;
        }
    }
    lock_data->second.group_lock_mode_ = max_lock_mode;
    }else{
        lock_data->second.group_lock_mode_ = GroupLockMode::NON_LOCK;
    }
    txn->set_state(TransactionState::SHRINKING);
    // std::cout<<"unlock后:"<<lock_data->second.group_lock_mode_<<std::endl;

    return true;
}
