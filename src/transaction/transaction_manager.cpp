/* Copyright (c) 2023 Renmin University of China
RMDB is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
        http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

#include "transaction_manager.h"
#include "record/rm_file_handle.h"
#include "system/sm_manager.h"

std::unordered_map<txn_id_t, Transaction *> TransactionManager::txn_map = {};

/**
 * @description: 事务的开始方法
 * @return {Transaction*} 开始事务的指针
 * @param {Transaction*} txn 事务指针，空指针代表需要创建新事务，否则开始已有事务
 * @param {LogManager*} log_manager 日志管理器指针
 */
Transaction * TransactionManager::begin(Transaction* txn, LogManager* log_manager) {
    // Todo:
    // 1. 判断传入事务参数是否为空指针
    if(txn == nullptr){
        // 2. 如果为空指针，创建新事务
        txn = new Transaction(next_txn_id_++);
        // txn = std::make_unique<Transaction>(next_txn_id_++).get();
    }

    // 3. 把开始事务加入到全局事务表中
    txn->set_start_ts(next_timestamp_++);
    txn_map[txn->get_transaction_id()] = txn;
    
    BeginLogRecord* beginLog = new BeginLogRecord(txn->get_transaction_id());
    beginLog->prev_lsn_ = txn->get_prev_lsn();
    log_manager->add_log_to_buffer(beginLog);
    txn->set_prev_lsn(beginLog->lsn_);

    delete beginLog;

    // 4. 返回当前事务指针
    return txn;
}

/**
 * @description: 事务的提交方法
 * @param {Transaction*} txn 需要提交的事务
 * @param {LogManager*} log_manager 日志管理器指针
 */
void TransactionManager::commit(Transaction* txn, LogManager* log_manager) {
    // Todo:
    // 1. 如果存在未提交的写操作，提交所有的写操作
    auto lockSet = txn->get_lock_set();

    // 2. 释放所有锁
    for(auto it = lockSet->begin(); it != lockSet->end(); it ++) {
        // std::cout<<"shifang!"<<std::endl;
        lock_manager_->unlock(txn, *it);
    }
    lockSet->clear();

    // 3. 释放事务相关资源，eg.锁集
    CommitLogRecord* commitLog = new CommitLogRecord(txn->get_transaction_id());
    commitLog->prev_lsn_ = txn->get_prev_lsn();
    log_manager->add_log_to_buffer(commitLog);
    txn->set_prev_lsn(commitLog->lsn_);

    // 4. 把事务日志刷入磁盘中
    log_manager->flush_log_to_disk();

    // 5. 更新事务状态
    txn->set_state(TransactionState::COMMITTED);
    delete commitLog;
}

/**
 * @description: 事务的终止（回滚）方法
 * @param {Transaction *} txn 需要回滚的事务
 * @param {LogManager} *log_manager 日志管理器指针
 */
void TransactionManager::abort(Transaction * txn, LogManager *log_manager) {
    // Todo:

    // 1. 回滚所有写操作
    if(txn==nullptr){
        return;
    }

    auto writeSet = txn->get_write_set();

    while(!writeSet->empty()) {
        
        auto &item = writeSet->back();

        auto context = new Context(lock_manager_, log_manager, txn);
        
        auto &file_handle = sm_manager_->fhs_.at(item->GetTableName());

        if(item->GetWriteType() == WType::INSERT_TUPLE) {

            auto rec = file_handle->get_record(item->GetRid(),context);

            for(const auto& index:sm_manager_->db_.get_table(item->GetTableName()).indexes) {
                
                std::string ix_name = sm_manager_->get_ix_manager()->get_index_name(item->GetTableName(),index.cols);
                auto &ix_handler = sm_manager_->ihs_.at(ix_name);
                int size = 0;
                int all_offset = 0;
                for(auto col:index.cols){
                    size+=col.len;
                }
                RmRecord record(size);
                for(auto col:index.cols){
                    memcpy(record.data+all_offset, rec->getData()+col.offset,col.len);
                    all_offset+=col.len;
                }
                ix_handler->delete_entry(record.data,txn);
            }
            file_handle->delete_record(item->GetRid(),context);

        }else if(item->GetWriteType() == WType::DELETE_TUPLE) {
            
            RmRecord old_rec = item->GetRecord();

            Rid rid = file_handle->insert_record(old_rec.data,context);
            for(const auto& index:sm_manager_->db_.get_table(item->GetTableName()).indexes) {
                std::string ix_name = sm_manager_->get_ix_manager()->get_index_name(item->GetTableName(),index.cols);
                auto &ix_handler = sm_manager_->ihs_.at(ix_name);
                int size = 0;
                int all_offset = 0;
                for(auto col:index.cols){
                    size+=col.len;
                }
                RmRecord record(size);
                for(auto col:index.cols){
                    memcpy(record.data+all_offset, old_rec.getData()+col.offset,col.len);
                    all_offset+=col.len;
                }
                ix_handler->insert_entry(record.data,rid,txn);
                
            }

        }else if(item->GetWriteType() == WType::UPDATE_TUPLE) {
            
            RmRecord old_rec = item->GetRecord();
            auto new_rec = file_handle->get_record(item->GetRid(),context);

            for(const auto& index:sm_manager_->db_.get_table(item->GetTableName()).indexes) {
                
                std::string ix_name = sm_manager_->get_ix_manager()->get_index_name(item->GetTableName(),index.cols);
                auto &ix_handler = sm_manager_->ihs_.at(ix_name);
                int size = 0;
                int all_offset = 0;
                for(auto col:index.cols){
                    size+=col.len;
                }
                RmRecord record(size);
                for(auto col:index.cols){
                    memcpy(record.data+all_offset, new_rec->getData()+col.offset,col.len);
                    all_offset+=col.len;
                }
                ix_handler->delete_entry(record.data,txn);
                
            }

            Rid rid = item->GetRid();
            file_handle->update_record(rid,old_rec.data,context);

            for(const auto& index:sm_manager_->db_.get_table(item->GetTableName()).indexes) {
                std::string ix_name = sm_manager_->get_ix_manager()->get_index_name(item->GetTableName(),index.cols);
                auto &ix_handler = sm_manager_->ihs_.at(ix_name);
                int size = 0;
                int all_offset = 0;
                for(auto col:index.cols){
                    size+=col.len;
                }
                RmRecord record(size);
                for(auto col:index.cols){
                    memcpy(record.data+all_offset, old_rec.getData()+col.offset,col.len);
                    all_offset+=col.len;
                }
                ix_handler->insert_entry(record.data,rid,txn);
            }
        }
        writeSet->pop_back();
    }

    writeSet->clear();

    // 2. 释放所有锁
    //std::cout<<"释放所有锁"<<std::endl;
    // auto lock_set = txn->get_lock_set();

    // for(auto iter = lock_set->begin(); iter != lock_set->end(); iter ++) {
    //     lock_manager_->unlock(txn, *iter);
    // }
    for (auto it = txn->get_lock_set()->begin(); it != txn->get_lock_set()->end(); it++) {
        // std::cout<<"进入循环"<<std::endl;
        lock_manager_->unlock(txn, *it);
    }
    
    // for(auto const &lock : *(txn->get_lock_set())) {
    //     lock_manager_->unlock(txn, lock);
    // }

    // 3. 清空事务相关资源，eg.锁集
    AbortLogRecord* abort_log = new AbortLogRecord(txn->get_transaction_id());
    abort_log->prev_lsn_ = txn->get_prev_lsn();
    log_manager->add_log_to_buffer(abort_log);
    txn->set_prev_lsn(abort_log->lsn_);


    // 4. 把事务日志刷入磁盘中
    log_manager->flush_log_to_disk();

    // 5. 更新事务状态
    txn->set_state(TransactionState::ABORTED);
}
